<?php
/**
 * Created by PhpStorm.
 * User: Rendol
 * Date: 30.11.2014
 * Time: 22:13
 */

namespace console\controllers;

use ext\WebSocket\Events;
use ext\WebSocket\Server;
use Yii;
use yii\console\Controller;

class WebSocketServerController extends Controller
{

	private $host;

	private $port;

	public function actionIndex()
	{
		defined('WEB_SOCKET_SERVER_DAEMON') or define('WEB_SOCKET_SERVER_DAEMON', false);
		$server = new Server();
		$server->init([
            'daemonMode' => WEB_SOCKET_SERVER_DAEMON,
            'host' => $this->host ?: Yii::$app->params['WebSocketServer-host'],
            'port' => $this->port ?: Yii::$app->params['WebSocketServer-port'],
            'events' => (new Events($server, '\ext\WebSocket\Commands'))->getAll()
        ])->run();
	}
} 