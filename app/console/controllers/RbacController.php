<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.10.2014
 * Time: 12:18
 */

namespace console\controllers;

use common\components\RbacHelper;
use common\models\User;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
	public function actionReset()
	{
		$auth = Yii::$app->authManager;

		$rbacDir = Yii::getAlias('@frontend/rbac');
		if (!is_dir($rbacDir)) {
			mkdir($rbacDir, 0755, true);
		}

		$rolePermissions = [];
		foreach (User::$roles as $roleId => $roleLabel) {
			$rolePermissions[$roleId] = array_keys($auth->getPermissionsByRole($roleId));
		}

		$userRoles = [];
		foreach (User::find()->all() as $user) {
			$userRoles[$user->id] = array_keys($auth->getAssignments($user->id));
		}


		$auth->removeAll();

		foreach (User::$roles as $roleId => $roleLabel) {
			$currentRole = $auth->createRole($roleId);
			$auth->add($currentRole);
		}

		foreach ($this->getTasks() as $task => $actions) {
			$currentRole = $auth->createPermission($task);
			$auth->add($currentRole);
			foreach ($actions as $action) {
				$currentPermission = $auth->createPermission($task . '/' . $action);
				$auth->add($currentPermission);
				$auth->addChild($currentRole, $currentPermission);
			}

			$auth->addChild($auth->getRole(User::ROLE_ADMIN), $currentRole);
		}

		if (!$auth->getAssignment(User::ROLE_GUEST, null)) {
			$auth->assign($auth->getRole(User::ROLE_GUEST), null);
		}
		if (!$auth->getAssignment(User::ROLE_ADMIN, 1)) {
			$auth->assign($auth->getRole(User::ROLE_ADMIN), 1);
		}

		foreach (User::$roles as $roleId => $roleLabel) {
			$role = $auth->getRole($roleId);
			foreach ($rolePermissions[$roleId] as $permissionName) {
				$permission = $auth->getPermission($permissionName);
				if ($permission && !$auth->hasChild($role, $permission)) {
					$auth->addChild($role, $permission);
				}
			}
		}

		foreach ($userRoles as $userId => $roles) {
			foreach ($roles as $roleId) {
				$role = $auth->getRole($roleId);
				if (!$auth->getAssignment($roleId, $userId)) {
					$auth->assign($role, $userId);
				}
			}
		}

		if (!User::findOne(1)) {
			$user = new User([
				'id' => 1,
				'username' => 'root',
				'status' => User::STATUS_ACTIVE
			]);
			$user->setPassword('admin');
			$user->save(false);
		}
	}

	protected function getTasks()
	{
		$controllerNamespace = Yii::$app->controllerNamespace;
		Yii::$app->controllerNamespace = 'frontend\\controllers';
		$tasks = RbacHelper::getTasks();
		Yii::$app->controllerNamespace = $controllerNamespace;
		return $tasks;
	}
} 