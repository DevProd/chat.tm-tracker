<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.10.2014
 * Time: 12:18
 */

namespace console\controllers;

use common\helpers\DbSync;
use Yii;
use yii\console\Controller;

class DbSyncController extends Controller
{
    public function actionIndex()
    {
        (new DbSync([
            'common\models',
        ]))->run();
    }
}