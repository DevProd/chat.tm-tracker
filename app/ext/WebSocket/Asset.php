<?php
namespace ext\WebSocket;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Asset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/chat/custom.js',
        'js/chat/web-socket.js',
        'js/chat/Application.js',
        'js/chat/Events.js',
    ];

    public $css = [
        'css/chat.css',
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];
}
