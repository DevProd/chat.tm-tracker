<?php
/**
 * Created by PhpStorm.
 * User: Alexey Veretelnyk
 * Date: 30.11.2014
 * Time: 22:13
 */

namespace ext\WebSocket;

use Exception;
use ext\Repository\Helper;
use Yii;

class Server
{
    /**
     * @var array Server configs
     */
    public $opts = [];

    /**
     * @var array Connected costumers
     */
    public $connections = [];

    /**
     * @var array The registration data for webHooks
     */
    public $webHooks = [];

    /**
     * @var array The registration data for customers
     */
    public $registers = [];

    /**
     * @var array The registration data for the chat room
     */
    public $chatRegisters = [];

    /**
     * @var mixed
     */
    public $lastBuf;

    /**
     * @param $config
     * @return $this
     */
    public function init($config = [])
    {
        Helper::runLogging(true);

        $this->opts = $config + [
                'daemonMode' => false,
                'host' => 'localhost',
                'port' => 8000,
                'sleep' => 50000, // = 50 milliseconds
                'events' => [
                    'connect' => function ($connection) {
                        /** @var resource $connection */
                    },
                    'message' => function ($connection, $data, $status) {
                        /** @var resource $connection */
                        /** @var array $data */
                        /** @var integer $status */
                    },
                    'iteration' => function ($connections) {
                        /** @var resource[] $connections */
                    },
                    'disconnect' => function ($connection) {
                        /** @var resource $connection */
                    },
                ]
            ];

        return $this;
    }

    public function run()
    {
        if ($this->opts['daemonMode']) {
            // Create child process
            // All code after pcntl_fork() will executed by two processes: parent and child
            $child_pid = pcntl_fork();
            if ($child_pid) {
                // Exit the parent, bound to a console process
                exit();
            }
            // Make a main process of a child of.
            posix_setsid();
        }

        $null = null;

        //Create socket
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        //Reusable port
        socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);

        //Bind a socket to a port
        socket_bind($socket, 0, $this->opts['port']);

        //Set port on listening
        socket_listen($socket);

        //create & add listening socket to the list
        $this->connections = array($socket);

        //Make an endless cycle of that did not stop our script
        while (true) {

            if (Yii::getLogger()->messages) {
                //foreach (Yii::getLogger()->messages as $message) {
                //	$this->sendMessage([
                //		'Log' => ['message' => $message]
                //	]);
                //}
                Yii::getLogger()->flush();
            }

            try {
                //Management of multiple connections
                $connections = $this->connections;

                //Returns an object of the socket in $connections array
                socket_select($connections, $null, $null, 0, 10);

                //Check, if a new connection is
                if (in_array($socket, $connections)) {
                    //Accept socket
                    $connection_new = socket_accept($socket);

                    //Add socket to an array of costumers
                    $this->connections[] = $connection_new;

                    //Getting data from connection
                    $header = socket_read($connection_new, 1024);

                    //Make a handshake
                    $this->_performHandshaking($header, $connection_new, $this->opts['host'], $this->opts['port']);

                    call_user_func($this->opts['events']['connect'], $connection_new);

                    //make room for new socket
                    $found_socket = array_search($socket, $connections);
                    unset($connections[$found_socket]);
                }

                //cycle for all connected sockets
                foreach ($connections as $connection) {
                    try {
                        //Check, whether there is a new message
                        while (@socket_recv($connection, $buf, 102400, 0) >= 1) {
                            //Decode it
                            $receivedData = $this->_unmask($buf);
                            $this->lastBuf = $data = @json_decode($receivedData, true);
                            call_user_func($this->opts['events']['message'], $connection, $data, ord($receivedData));

                            break 2; //Get out of this cycle
                        }

                        $buf = @socket_read($connection, 102400, PHP_NORMAL_READ);
                        // Check, if the message is empty
                        if ($buf === false) {

                            call_user_func($this->opts['events']['disconnect'], $connection);

                            // Remove the connection from the list of clients
                            $found_socket = array_search($connection, $this->connections);
                            @socket_getpeername($connection, $ip);
                            unset($this->connections[$found_socket]);

                        }
                    } catch (\Exception $e) {
                        $this->sendMessage(
                            [
                                'Exception' => [
                                    'lastBuf' => $this->lastBuf,
                                    'message' => $e->getMessage(),
                                    'code' => $e->getCode(),
                                    'trace' => $e->getTraceAsString(),
                                ]
                            ],
                            [
                                'connectionId' => intval($connection),
                                'publish' => true
                            ]
                        );
                    }
                }

                call_user_func($this->opts['events']['iteration'], $connections);

                // 50 milliseconds
                usleep($this->opts['sleep']);

            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
        // Close connection
        socket_close($socket);
    }

    /**
     * @param array $msg Сообщение
     * @param array $opts Options of a target for sending message
     * @return bool
     * @throws Exception
     */
    public function sendMessage($msg, $opts = [])
    {
        $o = $opts + [
                'connectionId' => null,
                'registerId' => null,
                'allowedIds' => [],
            ];

        $msg = $this->_mask(json_encode($msg));

        if (strlen($msg) >= 65536) {
            throw new Exception('Packet size "' . strlen($msg) . '". Limit: 65536');
        }

        foreach ($this->connections as $connection) {
            $cid = intval($connection);

            // Sent message by connection ID
            if (null !== $o['connectionId'] && $o['connectionId'] != $cid) {
                continue;
            }

            if (isset($this->registers[$cid])) {
                // Send message to allowed connections
                if (!empty($o['allowedIds']) && !in_array($this->registers[$cid], $o['allowedIds'])) {
                    continue;
                }
            }
            if (isset($this->chatRegisters[$cid])) {
                // Send message to the allowed group by chat ID
                if (!empty($o['chatIds']) && !in_array($this->chatRegisters[$cid], $o['chatIds'])) {
                    continue;
                }
            }

            //Helper::log('server.log', [$cid, $o, $msg]);

            // Send the message to the recipient
            @socket_write($connection, $msg, strlen($msg));
        }

        return true;
    }

    //Декодирование сообщения пришедшего от браузера
    private function _unmask($text)
    {
        $length = ord($text[1]) & 127;
        if ($length == 126) {
            $masks = substr($text, 4, 4);
            $data = substr($text, 8);
        } elseif ($length == 127) {
            $masks = substr($text, 10, 4);
            $data = substr($text, 14);
        } else {
            $masks = substr($text, 2, 4);
            $data = substr($text, 6);
        }
        $text = "";
        for ($i = 0; $i < strlen($data); ++$i) {
            $text .= $data[$i] ^ $masks[$i % 4];
        }
        return $text;
    }

    //Закодировать сообщение перед отправкой клиенту
    private function _mask($text)
    {
        $b1 = 0x80 | (0x1 & 0x0f);
        $length = strlen($text);

        $header = '';
        if ($length <= 125)
            $header = pack('CC', $b1, $length);
        elseif ($length > 125 && $length < 65536)
            $header = pack('CCn', $b1, 126, $length);
        elseif ($length >= 65536)
            $header = pack('CCN', $b1, 127, $length);
        return $header . $text;
    }

    //Делаем рукопожатие
    private function _performHandshaking($receved_header, $connection_conn, $host, $port)
    {
        $headers = array();
        $lines = preg_split("/\r\n/", $receved_header);
        foreach ($lines as $line) {
            $line = chop($line);
            if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
                $headers[$matches[1]] = $matches[2];
            }
        }

        $secKey = @$headers['Sec-WebSocket-Key'];
        $secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));

        //hand shaking header
        $upgrade = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
            "Upgrade: websocket\r\n" .
            "Connection: Upgrade\r\n" .
            "WebSocket-Origin: $host\r\n" .
            "Sec-WebSocket-Accept: $secAccept\r\n\r\n";
        socket_write($connection_conn, $upgrade, strlen($upgrade));
        unset($port);
    }
}