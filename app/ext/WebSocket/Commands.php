<?php
/**
 * Created by PhpStorm.
 * User: Rendol
 * Date: 30.11.2014
 * Time: 22:13
 */

namespace ext\WebSocket;

use common\models\Chat;
use common\models\ChatHasNotice;
use common\models\Notice;
use common\models\Repository;
use common\models\User;
use ext\Repository\Helper;
use ext\Repository\HookHelper;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;

class Commands
{
    /**
     * @var Server WebSocket server
     */
    public $server;

    /**
     * @var integer Connection id
     */
    public $connectionId;

    /**
     * @var integer
     */
    public $registerId;

    /**
     * @var Chat
     */
    public $chat;

    /**
     * @var User[]
     */
    public $users;

    /**
     * @var array
     */
    public $chatGroup = [];

    /**
     * @param $config
     */
    public function __construct($config = [])
    {
        foreach ($config as $param => $value) {
            $this->{$param} = $value;
        }
    }

    /**
     * @param array $data
     * @return array
     */
    public function run($data)
    {
        $res = null;
        if (isset($data['method'])) {
            $res = call_user_func_array(
                [$this, 'cmd' . $data['method']],
                isset($data['params']) ? $data['params'] : []
            );
        }
        return $res;
    }

    /**
     * @param $chat Chat
     * @return array
     * @throws Exception
     */
    public function getChatMemberIds($chat)
    {
        $allowedIds = [];
        $accessedIds = ArrayHelper::map($chat->getUserByAccess()->all(), 'id', 'id');

        if (isset($this->chatGroup[$chat->id])) {
            foreach ($this->chatGroup[$chat->id] as $userId) {
                if (in_array($userId, $accessedIds)) {
                    $allowedIds[$userId] = $userId;
                }
            }
        }

        return $allowedIds;
    }

    /**
     * @param $record ActiveRecord
     * @param string $scenario
     * @return array
     */
    public function getAttributesByScenario($record, $scenario = 'public')
    {
        $record->setScenario($scenario);
        return $record->getAttributes($record->safeAttributes());
    }

    /**
     * Connected
     *
     * @param $online bool
     * @return array
     * @throws Exception
     */
    public function cmdRegisterMember($online)
    {
        $params['id'] = $this->registerId;
        $params['online'] = $online;

        return [
            [
                'userInfo' => $params,
            ],
            [
                'allowedIds' => $this->getChatMemberIds($this->chat),
                'chatIds' => [$this->chat->id],
            ],
        ];
    }

    /**
     * Connected
     * @return array
     * @throws Exception
     */
    public function cmdChatInit()
    {
        if (!in_array($this->registerId, ArrayHelper::map($accessedUsers = $this->chat->getUserByAccess()->all(), 'id', 'id'))) {
            throw new Exception("Access denied", 404);
        }

        $notices = [];
        $userList = [];

        foreach ($accessedUsers as $k => $user) {
            $userList[$k] = $this->getAttributesByScenario($user);
            $userList[$k]['roleText'] = User::getRoles($userList[$k]['role']);
            $userList[$k]['online'] = in_array($userList[$k]['id'], $this->server->registers);
        }

        foreach (array_reverse($this->chat->getNotices()->orderBy('id DESC')->limit(20)->all()) as $k => $notice) {
            /**
             * @var $notice Notice
             * @var $repository Repository
             */
            $notices[$k]['id'] = $notice->id;
            $notices[$k]['message'] = nl2br($notice->message);
            $notices[$k]['type'] = $notice->type;
            $notices[$k]['typeText'] = Notice::getTypes($notice->type);
            $notices[$k]['created_at'] = date('Y-m-d H:i:s');

            if (!$notice->owner_id && ($repository = $this->chat->getRepository()->one())) {
                $notices[$k]['owner_id'] = null;
                $notices[$k]['ownerText'] = 'From repository: ' . $repository->name;
            } elseif ($owner = $notice->getOwnerOne()) {
                $notices[$k]['owner_id'] = $owner->id;
                $notices[$k]['ownerText'] = 'From: ' . $owner->username;
            }
        }

        return [
            [
                'noticeList' => $notices,
                'userList' => $userList
            ],
            [
                'connectionId' => $this->connectionId,
            ],
        ];
    }

    /**
     * Connected
     *
     * @param $text
     * @return array
     * @throws Exception
     */
    public function cmdSendMessage($text)
    {
        $allowedIds = $this->getChatMemberIds($this->chat);

        if (!in_array($this->registerId, $allowedIds)) {
            throw new Exception("Access denied for chat.", 404);
        }

        $transaction = Yii::$app->getDb()->beginTransaction();

        $notice = new Notice();
        $notice->owner_id = $this->registerId;
        $notice->type = Notice::TYPE_MESSAGE;
        $notice->message = $text;

        if ($saved = $notice->save()) {
            $chatNotice = new ChatHasNotice();
            $chatNotice->chat_id = $this->chat->id;
            $chatNotice->notice_id = $notice->id;

            $saved &= $chatNotice->save();
        }

        if (!$saved) {
            $transaction->rollBack();
            throw new Exception("Error.", 404);
        }

        $transaction->commit();

        $message = $this->getAttributesByScenario($notice, 'public');
        $message['typeText'] = Notice::getTypes($notice->type);
        $message['ownerText'] = 'From: ' . $this->users[$this->registerId]->username;
        $message['created_at'] = date('Y-m-d H:i:s');

        return [
            [
                'newNotices' => [$message],
            ],
            [
                'allowedIds' => $allowedIds,
                'chatIds' => [$this->chat->id],
            ],
        ];
    }

    /**
     * @param $firstId
     * @return array
     * @throws Exception
     */
    public function cmdGetPreviousMessage($firstId)
    {
        if (!in_array($this->registerId, ArrayHelper::map($accessedUsers = $this->chat->getUserByAccess()->all(), 'id', 'id'))) {
            throw new Exception("Access denied", 404);
        }

        $notices = [];

        foreach (
            $this->chat
                ->getNotices()
                ->where(['<', 'id', $firstId])
                ->orderBy('id DESC')
                ->limit(20)
                ->all()
            as $k => $notice) {
            /**
             * @var $notice Notice
             * @var $repository Repository
             */
            $notices[$k]['id'] = $notice->id;
            $notices[$k]['message'] = nl2br($notice->message);
            $notices[$k]['type'] = $notice->type;
            $notices[$k]['typeText'] = Notice::getTypes($notice->type);
            $notices[$k]['created_at'] = date('Y-m-d H:i:s');

            if (!$notice->owner_id && ($repository = $this->chat->getRepository()->one())) {
                $notices[$k]['owner_id'] = null;
                $notices[$k]['ownerText'] = 'From repository: ' . $repository->name;
            } elseif ($owner = $notice->getOwnerOne()) {
                $notices[$k]['owner_id'] = $owner->id;
                $notices[$k]['ownerText'] = 'From: ' . $owner->username;
            }
        }

        return [
            [
                'prevNoticeList' => $notices,
            ],
            [
                'connectionId' => $this->connectionId,
            ],
        ];
    }

    /**
     * Connected
     *
     * @param $alias
     * @param $fileName
     * @return array
     * @throws Exception
     */
    public function cmdHookFileCache($alias, $fileName)
    {
        if (!($cache = Helper::getFileContent($fileName, true)) || !Helper::removeFile($fileName)) {
            throw new Exception("Cache file not found.", 404);
        }

        if (!($repository = $this->_getRepositoryByAlias($alias))) {
            throw new Exception("Repository not found.", 404);
        }

        if (!($project = $repository->getProject())) {
            throw new Exception("Project not found.", 404);
        }

        $chats = $project->getChats();
        $notices = [];
        $allowedUsers = [];
        $saved = true;

        $transaction = Yii::$app->getDb()->beginTransaction();

        foreach (HookHelper::getData($cache) as $val) {
            if (isset($val['commits'])) {
                foreach ($val['commits'] as $k => $commit) {
                    $notice = new Notice();
                    $notice->owner_id = null;
                    $notice->type = Notice::TYPE_COMMIT;
                    $notice->message = "{$commit['message']};" . PHP_EOL . "<b>Branch: {$val['branch']}</b>";

                    if ($saved &= $notice->save()) {

                        foreach ($chats as $chat) {
                            if ($chat->enable_repository_events) {
                                $chatNotice = new ChatHasNotice();
                                $chatNotice->chat_id = $chat->id;
                                $chatNotice->notice_id = $notice->id;

                                $allowedUsers = $allowedUsers + $this->getChatMemberIds($chat);

                                $saved &= $chatNotice->save();
                            }
                        }

                        $notices[$k]['id'] = $notice->id;
                        $notices[$k]['message'] = nl2br($notice->message);
                        $notices[$k]['type'] = Notice::TYPE_COMMIT;
                        $notices[$k]['typeText'] = Notice::getTypes(Notice::TYPE_COMMIT) . ' ' . Html::a(
                                '<span class="glyphicon glyphicon-link"></span>',
                                $commit['link'],
                                ['target' => '_blank']
                            );
                        $notices[$k]['owner_id'] = null;
                        $notices[$k]['ownerText'] = 'From repository: ' . $repository->name;
                        $notices[$k]['created_at'] = date('Y-m-d H:i:s');
                    }
                }
            }
        }

        if ($saved) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
        }

        return [
            [
                'newNotices' => $notices,
            ],
            [
                'allowedIds' => $allowedUsers,
                'charIds' => ArrayHelper::map($chats, 'id', 'id'),
            ],
        ];
    }

    /**
     * @param $alias
     * @return Repository
     * @throws NotFoundHttpException
     */
    private function _getRepositoryByAlias($alias)
    {
        if (!$alias || !($rec = Repository::find()->where(['alias' => htmlspecialchars($alias)])->one())) {
            throw new NotFoundHttpException("Access denied", 404);
        };

        return $rec;
    }
}