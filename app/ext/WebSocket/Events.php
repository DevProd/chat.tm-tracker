<?php
/**
 * Created by PhpStorm.
 * User: Rendol
 * Date: 30.11.2014
 * Time: 22:13
 */

namespace ext\WebSocket;

use common\models\Chat;
use common\models\User;
use Exception;
use ext\Repository\Helper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class Events
{
    /**
     * @var Server
     */
    public $server;

    /**
     * @var string
     */
    public $cmdClass;

    /**
     * @var Chat[]
     */
    public $chats;

    /**
     * @var User[]
     */
    public $users;

    /**
     * @var array
     */
    public $chatGroup;

    /**
     * @param $server Server
     * @param $cmdClass string
     */
    public function __construct($server, $cmdClass)
    {
        $this->server = $server;
        $this->cmdClass = $cmdClass;
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return [
            'connect' => [$this, 'eventConnect'],
            'register' => [$this, 'eventRegister'],
            'message' => [$this, 'eventMessage'],
            'iteration' => [$this, 'eventIteration'],
            'disconnect' => [$this, 'eventDisconnect'],
        ];
    }

    /**
     * @param resource[] $connections
     */
    public function eventIteration($connections)
    {

    }

    /**
     * @param resource $connection
     */
    public function eventConnect($connection)
    {

    }

    /**
     * @param resource $connection
     * @param array $data
     * @param integer $status
     * @throws Exception
     */
    public function eventMessage($connection, $data, $status)
    {
        $connectionId = intval($connection);

        if ($status == 3) {
            $this->eventDisconnect($connection);
        } elseif (isset($data['hookFileCache'])) {
            $this->eventWebHook('hookFileCache', $data['hookFileCache'], $connectionId);
        } else {
            if (!isset($this->server->registers[$connectionId]) && isset($data['register'])) {
                $this->eventRegister($connection, $data['register']);
            } else {
                $this->runCommand($data, $connectionId);
            }

            if (isset($data['callback'])) {
                $this->server->sendMessage(
                    [
                        'callback' => $data['callback'],
                    ],
                    [
                        'connectionId' => $connectionId,
                    ]
                );
            }
        }
    }

    /**
     * @param resource $connection
     * @param array $data
     * @throws Exception
     */
    public function eventRegister($connection, $data)
    {
        $connectionId = intval($connection);

        if (!isset($data['registerId']) || !isset($data['chatId'])) {
            throw new Exception('Not found required attributes for register!');
        }
        $registerId = $data['registerId'];
        $chatId = $data['chatId'];

        $this->server->registers[$connectionId] = $registerId;
        $this->server->chatRegisters[$connectionId] = $chatId;

        $this->runCommand([
            'method' => 'registerMember',
            'params' => [true],
        ], $connectionId, $registerId, $chatId);
    }

    /**
     * @param resource $connection
     */
    public function eventDisconnect($connection)
    {
        $connectionId = intval($connection);
        if (isset($this->server->webHooks[$connectionId])) {
            unset($this->server->webHooks[$connectionId]);
        } elseif (isset($this->server->registers[$connectionId], $this->server->chatRegisters[$connectionId])) {
            $registerId = $this->server->registers[$connectionId];
            $chatId = $this->server->chatRegisters[$connectionId];
            unset($this->server->registers[$connectionId]);
            unset($this->server->chatRegisters[$connectionId]);
            $this->runCommand([
                'method' => 'registerMember',
                'params' => [false],
            ], $connectionId, $registerId, $chatId);
        }
    }

    /**
     * @param array $data
     * @param integer $connectionId
     * @param $registerId
     * @param $chatId
     * @throws Exception
     */
    public function runCommand($data, $connectionId, $registerId = null, $chatId = null)
    {
        Yii::$app->db->open();

        if (isset($this->server->registers[$connectionId]) && empty($registerId)) {
            $registerId = $this->server->registers[$connectionId];
        }

        if (isset($this->server->chatRegisters[$connectionId]) && empty($chatId)) {
            $chatId = $this->server->chatRegisters[$connectionId];
        }

        $user = $this->_getUser($registerId);

        if(!($chat = Chat::findOne($chatId))) {
            throw new \Exception('Chat not found');
        }

        $this->chatGroup[$chat->id][$connectionId] = $user->id;

        /** @var Commands $cmd */
        $cmd = (new $this->cmdClass([
            'server' => $this->server,
            'connectionId' => $connectionId,
            'registerId' => $user->id,
            'chat' => $chat,
            'users' => $this->users,
            'chatGroup' => $this->chatGroup,
        ]));

        if (!isset($this->server->registers[$connectionId]) && isset($this->users[$registerId])) {
            unset($this->users[$registerId]);
            if (isset($this->chatGroup[$chat->id], $this->chatGroup[$chat->id][$connectionId])) {
                unset($this->chatGroup[$chat->id][$connectionId]);
            }
        }

        if (isset($this->chatGroup[$chat->id]) && empty($this->chatGroup[$chat->id])) {
            unset($this->chatGroup[$chat->id]);
        }

        $params = $cmd->run($data);

        Yii::$app->db->close();

        if (null !== $params) {
            call_user_func_array([$this->server, 'sendMessage'], $params);
        }
    }

    /**
     * @param $method
     * @param $data
     * @param $connectionId
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function eventWebHook($method, $data, $connectionId)
    {
        if (!isset($data['alias']) || !isset($data['fileName'])) {
            throw new Exception('Not found required attributes for WebHook!');
        }

        if (!isset($this->server->webHooks[$connectionId])) {
            $this->server->webHooks[$connectionId] = $data['alias'];
        }

        /** @var Commands $cmd */
        $cmd = (new $this->cmdClass([
            'server' => $this->server,
            'connectionId' => $connectionId,
            'users' => $this->users,
            'chatGroup' => $this->chatGroup,
        ]));

        Yii::$app->db->open();

        $params = $cmd->run([
            'method' => $method,
            'params' => $data,
        ]);

        Yii::$app->db->close();

        if ($params) {
            call_user_func_array([$this->server, 'sendMessage'], $params);
        }
    }

    /**
     * @param $registerId
     * @return User
     * @throws Exception
     */
    private function _getUser($registerId)
    {
        $user = null;

        if (null !== $registerId) {
            if (isset($this->users[$registerId])) {
                $user = $this->users[$registerId];
            } else {
                $this->users[$registerId] = $user = User::findOne($registerId);
            }
        }

        if (null === $user) {
            throw new \Exception('User not found');
        }

        return $user;
    }
}
