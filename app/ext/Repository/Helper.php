<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 10.11.2015
 * Time: 2:19
 */

namespace ext\Repository;

use yii\helpers\VarDumper;

class Helper
{
    private static $logging = false;

    private static $tempAlias = '@frontend/runtime/temp-repository-data/';
    private static $logAlias = '@frontend/runtime/socket-log/';

    /**
     * @param $logging bool
     */
    public static function runLogging($logging)
    {
        static::$logging = (bool)$logging;
    }

    /**
     * @param $data
     * @param $key
     * @param $chars
     * @return mixed
     */
    public static function trimByKey($data, $key, $chars)
    {
        foreach ($data as $k => $v) {
            if (is_array($v)) {
                $data[$k] = static::trimByKey($v, $key, $chars);
            } elseif ($key === $k) {
                $data[$k] = trim($v, $chars);
            }
        }

        return $data;
    }

    /**
     * @param $string string
     * @return string
     */
    public static function cacheData($string)
    {
        $path = \Yii::getAlias(static::$tempAlias);
        $fileName = uniqid(sha1(microtime(true))) . '_' . strlen($string) . '.json';

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        file_put_contents($path . $fileName, $string);

        return $fileName;
    }

    /**
     * @param $fileName string
     * @param $decode bool
     * @return mixed
     */
    public static function getFileContent($fileName, $decode = false)
    {
        $path = \Yii::getAlias(static::$tempAlias) . $fileName;

        if (!is_file($path)) {
            return false;
        }

        return ($content = file_get_contents($path)) && $decode ? json_decode($content, true) : $content;
    }

    /**
     * @param $fileNam string
     * @return bool
     */
    public static function removeFile($fileNam)
    {
        $path = \Yii::getAlias(static::$tempAlias) . $fileNam;

        if (!is_file($path)) {
            return false;
        }

        return unlink($path);
    }

    /**
     * @param $fileName
     * @param $data
     * @param int $append
     * @return mixed
     */
    public static function log($fileName, $data, $append = FILE_APPEND)
    {
        if (static::$logging === false) {
            return;
        }

        $path = \Yii::getAlias(static::$logAlias);

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        if (is_array($data)) {
            $data = strcmp($fileName, '.json') === 0
                ? json_encode($data)
                : VarDumper::export($data);
        }

        $eol = $append == FILE_APPEND ? PHP_EOL . PHP_EOL : null;

        file_put_contents($path . $fileName, ($eol ? date('Y-m-d H:i:s') : '') . $eol . $data . $eol, $append);
    }
}