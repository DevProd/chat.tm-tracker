<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 10.11.2015
 * Time: 6:07
 */

namespace ext\Repository;


use common\models\Repository;

class HookHelper
{

    private static $_commitTpl = [

    ];

    /**
     * @param $response
     * @param bool $asArray
     * @return array|object
     */
    static function getData($response, $asArray = true)
    {
        $out = [];

        if($response['type'] == Repository::REPOSITORY_BITBUCKET) {
            if (isset($response['push'], $response['push']['changes'])) {
                foreach ($response['push']['changes'] as $k => $change) {
                    $out[$k] = [];

                    if (isset($change['new'], $change['new']['name'])) {
                        $out[$k]['branch'] = $change['new']['name'];
                    }

                    if (isset($change['commits']) && is_array($change['commits'])) {
                        $commits = [];

                        foreach ($change['commits'] as $n => $commit) {
                            $commits[$n]['hash'] = $commit['hash'];
                            $commits[$n]['message'] = $commit['message'];
                            $commits[$n]['author'] = $commit['author']['raw'];
                            $commits[$n]['link'] = $commit['links']['html']['href'];
                        }
                        $out[$k]['commits'] = array_reverse($commits);
                    }
                }
            }
        } elseif ($response['type'] == Repository::REPOSITORY_GITHUB) {
            $out[0] = [];

            if (isset($response['repository'], $response['repository']['master_branch'])) {
                $out[0]['branch'] = $response['repository']['master_branch'];
            }

            if(isset($response['commits'])) {
                $commits = [];

                foreach($response['commits'] as $k => $commit) {
                    $commits[$k]['message'] = $commit['message'];
                    $commits[$k]['hash'] = $commit['id'];
                    $commits[$k]['author'] = $commit['author']['username'];
                    $commits[$k]['link'] = $commit['url'];
                }

                $out[0]['commits'] = $commits;
            }
        }

        return $asArray ? $out : (object)$out;
    }
} 