<?php
/**
* Auto-generated file, do not modify!
*/

use yii\db\Schema;
use yii\db\Migration;

class m141114_111739_create_user extends Migration
{
    public function safeUp()
    {
		$this->createTable('{{%user}}', [
			'id' => Schema::TYPE_PK,
			'username' => Schema::TYPE_STRING,
			'email' => Schema::TYPE_STRING,
			'password_hash' => Schema::TYPE_STRING,
			'password_reset_token' => Schema::TYPE_STRING,
			'auth_key' => Schema::TYPE_STRING,
			'status' => Schema::TYPE_INTEGER,
			'updated_at' => Schema::TYPE_TIMESTAMP,
			'created_at' => Schema::TYPE_TIMESTAMP,
			]);
    }

    public function down()
    {
		$this->dropTable('{{%user}}');
	}
}
