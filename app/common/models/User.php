<?php
namespace common\models;

use common\helpers\ModelHelper;
use common\helpers\ModelHelperSearchData;
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Schema;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $presentChatId
 * @property array $presentChatIds
 */
class User extends base\User implements IdentityInterface
{
    const ROLE_ADMIN = 1;
    const ROLE_DEVELOPER = 2;
    const ROLE_CUSTOMER = 3;
    const ROLE_MODERATOR = 4;

    /**
     * @param null $role
     * @return array|null
     */
    public static function getRoles($role = null)
    {
        $roles = [
            self::ROLE_ADMIN => Yii::t('roles', 'Administrator'),
            self::ROLE_MODERATOR => Yii::t('roles', 'Moderator'),
            self::ROLE_DEVELOPER => Yii::t('roles', 'Developer'),
            self::ROLE_CUSTOMER => Yii::t('roles', 'Customer'),
        ];

        if (null !== $role) {
            return isset($roles[$role]) ? $roles[$role] : null;
        }

        return $roles;
    }

    private $_userRoles;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'role', 'password_hash'], 'required', 'on' => 'sign-up'],
            [['email'], 'email', 'on' => 'sign-up'],
            [['role'], 'integer', 'on' => 'sign-up'],
            [['id', 'username', 'email', 'role'], 'safe', 'on' => 'public'],
            [['email'], 'safe', 'on' => 'access-management'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'userRoles' => 'Роли'
            ]
        );
    }

    /**
     * @return ActiveQuery
     */
    static function getActive()
    {
        return static::find()->where(['=', 'status', static::STATUS_ACTIVE]);
    }

    public function getPassword()
    {
        return '';
    }

    public function setUserRoles($value)
    {
        $this->_userRoles = $value;
    }

    public function getUserRoles()
    {
        return $this->_userRoles
            ? $this->_userRoles
            : ArrayHelper::map(Yii::$app->authManager->getRolesByUser($this->id), 'name', 'name');
    }

    /**
     * @param string|self $model
     * @return ModelHelperSearchData
     */
    static function search($model = __CLASS__)
    {
        if (is_string($model)) {
            $model = new $model;
        }

        $query = ModelHelper::searchQuery($model, \Yii::$app->request->get());

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return (object)[
            'filterModel' => $model,
            'dataProvider' => $dataProvider
        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    static function findAllActive()
    {
        return static::find()->where(['=', 'status', static::STATUS_ACTIVE])->all();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        if (!empty($password)) {
            $this->password_hash = Yii::$app->security->generatePasswordHash($password);
        }
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * @return ActiveQuery
     */
    public function getHasProject()
    {
        return $this->hasMany(ProjectHasUser::className(), ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getHasChat()
    {
        return $this->hasMany(ChatHasUser::className(), ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getChats()
    {
        if (!$this->currentChatId) {
            //return null;
        }

        return $this->hasMany(Chat::className(), ['id' => 'chat_id'])->via('hasChat');
    }
}
