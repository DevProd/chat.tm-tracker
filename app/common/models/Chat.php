<?php
namespace common\models;

use common\models\ext\AR;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Schema;

/**
 * Chat model
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $owner_id
 * @property string $name
 * @property string $description
 * @property integer $status
 * @property boolean $enable_repository_events
 * @property string $updated_at
 * @property string $created_at
 *
 * @property integer $connectedUserId
 * @property array $connectedUserIds
 */
class Chat extends AR
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_chat}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'project_id' => Schema::TYPE_INTEGER,
            'owner_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'enable_repository_events' => Schema::TYPE_BOOLEAN,
            'status' => Schema::TYPE_SMALLINT,
            'updated_at' => Schema::TYPE_TIMESTAMP,
            'created_at' => Schema::TYPE_TIMESTAMP,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'project_id' => 'Project',
            'owner_id' => 'Пользователь',
            'name' => 'Имя',
            'description' => 'Контент',
            'enable_repository_events' => 'Notify repository events',
            'status' => 'Статус',
            'updated_at' => 'Обновлено',
            'created_at' => 'Добавлено',
        ];
    }

    /**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			[
				'class' => TimestampBehavior::className(),
				'value' => new Expression('NOW()')
			]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'description', 'enable_repository_events', 'status', 'created_at'], 'safe'],
			[['name'], 'required', 'on' => 'create-chat'],
		];
	}

    /**
     * @return ActiveQuery
     */
    public function getHasNotices()
    {
        return $this->hasMany(ChatHasNotice::className(), ['chat_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getNotices()
    {
        return $this->hasMany(Notice::className(), ['id' => 'notice_id'])->via('hasNotices');
    }

    /**
     * @return ActiveQuery
     */
    public function getRepository()
    {
        return $this->hasOne(Repository::className(), ['project_id' => 'project_id']);
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id'])->one();
    }

    /**
     * @return ActiveQuery
     */
    public function getHasUser()
    {
        return $this->hasMany(ChatHasUser::className(), ['chat_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserByAccess()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->via('hasUser');
    }
}
