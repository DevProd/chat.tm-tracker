<?php
namespace common\models;

use common\models\ext\AR;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Schema;

/**
 * ChatHasNotice model
 *
 * @property integer $id
 * @property integer $chat_id
 * @property integer $notice_id
 */
class ChatHasNotice extends AR
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_chat_has_notice}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'chat_id' => Schema::TYPE_INTEGER,
            'notice_id' => Schema::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'chat_id' => 'Chat',
            'notice_id' => 'Notice',
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['chat_id', 'notice_id'], 'safe'],
		];
	}
}
