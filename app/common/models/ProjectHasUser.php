<?php
namespace common\models;

use common\models\ext\AR;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Schema;

/**
 * ProjectHasUser model
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $project_id
 */
class ProjectHasUser extends AR
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_project_has_user}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'project_id' => Schema::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'user_id' => 'Chat',
            'project_id' => 'Notice',
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'project_id'], 'safe'],
		];
	}
}
