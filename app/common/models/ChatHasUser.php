<?php
namespace common\models;

use common\models\ext\AR;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Schema;

/**
 * ChatHasUser model
 *
 * @property integer $id
 * @property integer $chat_id
 * @property integer $user_id
 */
class ChatHasUser extends AR
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_chat_has_user}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'chat_id' => Schema::TYPE_INTEGER,
            'user_id' => Schema::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'chat_id' => 'Chat',
            'user_id' => 'Пользователь',
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['chat_id', 'user_id'], 'safe'],
		];
	}
}
