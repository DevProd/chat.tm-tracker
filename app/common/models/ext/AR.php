<?php
namespace common\models\ext;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

/**
 * AR model
 */
class AR extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_ARCHIVED = 2;
    const STATUS_DELETED = 3;

    /**
     * @param null $status
     * @return array
     */
    public static function getStatuses($status = null)
    {
        $statuses = [
            static::STATUS_ACTIVE => 'Active',
            static::STATUS_ARCHIVED => 'Archived',
            static::STATUS_DELETED => 'Deleted',
        ];

        if(null !== $status) {
            return ArrayHelper::getValue($statuses, $status, null);
        }

        return $statuses;
    }

    /**
     * @param $id
     * @return static
     * @throws BadRequestHttpException
     */
    public static function findById($id)
    {
        if (!$id || null === ($model = static::findOne($id))) {
            throw new BadRequestHttpException("Object not find!", 404);
        }

        return $model;
    }

    /**
     * @inheritdoc
     */
    public function attributeTypes()
    {
        return [];
    }
}
