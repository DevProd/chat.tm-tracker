<?php
namespace common\models;

use common\models\ext\AR;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Schema;

/**
 * Repository model
 *
 * @property integer $id
 * @property integer $owner_id
 * @property integer $project_id
 * @property string $uuid
 * @property string $name
 * @property string $alias
 * @property string $description
 * @property string $repository_url
 * @property integer $type
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 */
class Repository extends AR
{
    const TYPE_MERCURIAL = 1;
    const TYPE_GIT = 2;
    const TYPE_SUBVERSION = 3;

    const REPOSITORY_GITHUB = 1;
    const REPOSITORY_BITBUCKET = 2;

    /**
     * @param null $type
     * @return array|null
     */
    static function getTypes($type = null)
    {
        $arr = [
            static::TYPE_MERCURIAL => 'Mercurial',
            static::TYPE_GIT => 'Git',
            static::TYPE_SUBVERSION => 'Subversion',
        ];

        if (null !== $type) {
            if (isset($arr[$type])) {
                return $arr[$type];
            }

            return null;
        }

        return $arr;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_repository}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'owner_id' => Schema::TYPE_INTEGER,
            'project_id' => Schema::TYPE_INTEGER,
            'uuid' => Schema::TYPE_STRING,
            'name' => Schema::TYPE_STRING,
            'alias' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'repository_url' => Schema::TYPE_STRING,
            'type' => Schema::TYPE_SMALLINT,
            'status' => Schema::TYPE_SMALLINT,
            'updated_at' => Schema::TYPE_TIMESTAMP,
            'created_at' => Schema::TYPE_TIMESTAMP,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'owner_id' => 'Owner',
            'project_id' => 'Project',
            'uuid' => 'Uuid',
            'name' => 'Name',
            'alias' => 'Alias',
            'description' => 'Description',
            'repository_url' => 'Link to repository',
            'type' => 'Repository type',
            'status' => 'Status',
            'updated_at' => 'Updated',
            'created_at' => 'Created',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'safe'],
            [['alias', 'name', 'repository_url', 'type'], 'required'],
            [['alias', 'name', 'repository_url'], 'unique'],
            [['repository_url'], 'url'],
        ];
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->hasOne('\common\models\Project', ['id' => 'project_id'])->one();
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->hasOne('\common\models\User', ['id' => 'owner_id'])->one();
    }
}