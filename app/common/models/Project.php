<?php
namespace common\models;

use common\models\ext\AR;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Schema;

/**
 * Project model
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $name
 * @property string $description
 * @property integer $status
 * @property string $updated_at
 * @property string $created_at
 */
class Project extends AR
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_project}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'owner_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'status' => Schema::TYPE_SMALLINT,
            'updated_at' => Schema::TYPE_TIMESTAMP,
            'created_at' => Schema::TYPE_TIMESTAMP,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'owner_id' => Yii::t('project', 'User'),
            'name' => Yii::t('project', 'Name'),
            'description' => Yii::t('project', 'Description'),
            'status' => Yii::t('project', 'Status'),
            'updated_at' => Yii::t('project', 'Updated at'),
            'created_at' => Yii::t('project', 'Created at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'status', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->hasOne('\common\models\User', ['id' => 'owner_id'])->one();
    }

    /**
     * @return Chat[]
     */
    public function getChats()
    {
        return $this->hasOne('\common\models\Chat', ['project_id' => 'id'])->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getHasUser()
    {
        return $this->hasMany(ProjectHasUser::className(), ['project_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->via('hasUser');
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert = true)
    {
        $this->status = Project::STATUS_ACTIVE;
        $this->owner_id = Yii::$app->getUser()->getId();

        return parent::beforeSave($insert);
    }
}
