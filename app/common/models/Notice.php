<?php
namespace common\models;

use common\models\ext\AR;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Schema;

/**
 * Notification model
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $message
 * @property integer $type
 * @property string $updated_at
 * @property string $created_at
 */
class Notice extends AR
{
    const TYPE_MESSAGE = 1;
    const TYPE_COMMIT = 2;
    const TYPE_TASK_EVENT = 3;
    const TYPE_USER_EVENT = 4;

    /**
     * @param null $type
     * @return array|string
     */
    public static function getTypes($type = null)
    {
        $types = [
            static::TYPE_MESSAGE => 'Message',
            static::TYPE_COMMIT => 'Commit',
            static::TYPE_TASK_EVENT => 'Task event',
            static::TYPE_USER_EVENT => 'User event',
        ];

        if($type !== null && isset($types[$type])) {
            return $types[$type];
        }

        return $types;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_notice}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'owner_id' => Schema::TYPE_INTEGER,
            'message' => Schema::TYPE_TEXT,
            'type' => Schema::TYPE_SMALLINT,
            'updated_at' => Schema::TYPE_TIMESTAMP,
            'created_at' => Schema::TYPE_DATETIME,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'owner_id' => 'Пользователь',
            'message' => 'Message',
            'type' => 'Type',
            'updated_at' => 'Обновлено',
            'created_at' => 'Добавлено',
        ];
    }

    /**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			[
				'class' => TimestampBehavior::className(),
				'value' => new Expression('NOW()')
			]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['owner_id', 'message', 'type'], 'safe'],
			[['id', 'owner_id', 'message', 'type'], 'safe', 'on' => 'public'],
		];
	}

    /**
     * @return User
     */
    public function getOwnerOne()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id'])->one();
    }
}
