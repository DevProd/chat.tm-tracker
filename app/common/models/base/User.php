<?php
/**
* Auto-generated file, do not modify!
*/

namespace common\models\base;

use common\models\ext\AR;
use Yii;
use yii\db\Schema;

/**
 * Class User
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $status
 * @property integer $role
 * @property string $last_activity
 * @property string $updated_at
 * @property string $created_at
 */
class User extends AR
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%tbl_user}}';
	}

	/**
	 * @inheritdoc
	 */
	public function attributeTypes()
	{
		return [
			'id' => Schema::TYPE_PK,
			'username' => Schema::TYPE_STRING,
			'email' => Schema::TYPE_STRING,
			'password_hash' => Schema::TYPE_STRING,
			'password_reset_token' => Schema::TYPE_STRING,
			'status' => Schema::TYPE_INTEGER,
			'role' => Schema::TYPE_INTEGER,
			'auth_key' => Schema::TYPE_STRING,
			'last_activity' => Schema::TYPE_TIMESTAMP,
			'updated_at' => Schema::TYPE_TIMESTAMP,
			'created_at' => Schema::TYPE_DATETIME,
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => '#',
			'username' => 'Имя',
			'email' => 'Email',
			'password_hash' => 'Пароль',
			'password_reset_token' => 'Токен сброса пароля',
			'auth_key' => 'Ключ авторизации',
			'status' => 'Статус',
			'role' => 'Роль',
			'last_activity' => 'Last activity',
			'updated_at' => 'Обновлено',
			'created_at' => 'Добавлено',
		];
	}

}