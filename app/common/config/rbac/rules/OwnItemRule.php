<?php

namespace common\config\rbac\rules;

use common\models\Chat;
use common\models\User;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\rbac\Item;
use yii\rbac\Rule;
use Yii;

class OwnItemRule extends Rule
{
    /** @var string */
    public $name = 'isOwner';

    /**
     * @param int $userId
     * @param Item $item
     * @param array $params
     * @return bool
     */
    public function execute($userId, $item, $params)
    {
        $roles = Yii::$app->getAuthManager()->getRolesByUser($userId);
        if (isset($roles[User::ROLE_ADMIN]) || isset($roles[User::ROLE_MODERATOR])) {
            return true;
        }

        if (!isset($params['attribute']) || !isset($params['model']) || !$params['model'] instanceof ActiveRecord) {

            if (!empty($item->data) && isset($item->data['model'], $item->data['attribute'])) {
                /** @var ActiveRecord $cls */
                $cls = $item->data['model'];

                return null !== ($id = Yii::$app->getRequest()->get('id')) && null !== $cls::find()->where([
                    'id' => $id,
                    $item->data['attribute'] => $userId,
                ])->one();
            }

            return false;
        }

        /** @var ActiveRecord $model */
        $model = $params['model'];
        return $model->{$params['attribute']} == $userId;
    }
}
