<?php
return [
    1 => [
        'type' => 1,
        'description' => 'Administrator',
        'children' => [
            'repository/create',
            'project',
            'chat',
            'user',
        ],
    ],
    2 => [
        'type' => 1,
        'description' => 'Developer',
        'children' => [
            'project/index',
            'project/view',
            'project/create-chat',
            'chat',
            'repository/create',
        ],
    ],
    3 => [
        'type' => 1,
        'description' => 'Customer',
        'children' => [
            'chat',
            'project/index',
            'project/create',
            'project/update',
            'project/view',
            'project',
        ],
    ],
    4 => [
        'type' => 1,
        'description' => 'Moderator',
        'children' => [
            'repository/create',
            'chat',
            'project',
            'user',
        ],
    ],
    '?' => [
        'type' => 1,
        'children' => [
            'user/sign-up',
            'repository/hook',
        ],
    ],
    'chat' => [
        'type' => 2,
        'children' => [
            'chat/index',
            'chat/grand-access',
            'chat/manage-access',
        ],
    ],
    'chat/index' => [
        'type' => 2,
    ],
    'chat/grand-access' => [
        'type' => 2,
        'ruleName' => 'isOwner',
        'data' => [
            'attribute' => 'owner_id',
            'model' => 'common\\models\\Chat',
        ],
    ],
    'chat/manage-access' => [
        'type' => 2,
        'ruleName' => 'isOwner',
        'data' => [
            'attribute' => 'owner_id',
            'model' => 'common\\models\\Chat',
        ],
    ],
    'project' => [
        'type' => 2,
        'children' => [
            'project/index',
            'project/create',
            'project/update',
            'project/view',
            'project/create-chat',
        ],
    ],
    'project/index' => [
        'type' => 2,
    ],
    'project/create' => [
        'type' => 2,
    ],
    'project/update' => [
        'type' => 2,
        'ruleName' => 'isOwner',
        'data' => [
            'attribute' => 'owner_id',
            'model' => 'common\\models\\Project',
        ],
    ],
    'project/view' => [
        'type' => 2,
    ],
    'project/create-chat' => [
        'type' => 2,
    ],
    'repository' => [
        'type' => 2,
        'children' => [
            'repository/create',
            'repository/hook',
        ],
    ],
    'repository/create' => [
        'type' => 2,
    ],
    'repository/hook' => [
        'type' => 2,
    ],
    'user' => [
        'type' => 2,
        'children' => [
            'user/index',
            'user/sign-up',
        ],
    ],
    'user/index' => [
        'type' => 2,
    ],
    'user/sign-up' => [
        'type' => 2,
    ],
];
