<?php
return [
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'language' => 'ru_RU',
	'timeZone' => 'UTC',
	'components' => [
		'db' => [
			'class' => 'yii\db\Connection',
			'enableSchemaCache' => true,
			'enableQueryCache' => true,
			'queryCacheDuration' => 1,
			'charset' => 'utf8',
			'on afterOpen' => function($event) {
				$event->sender->createCommand("SET time_zone = '".date('P')."'")->execute();
			},
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'i18n' => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@frontend/messages',
					'sourceLanguage' => 'system',
					'fileMap' => [
						//'main' => 'main.php',
					],
				],
			],
		],
		'console' => [
			'class' => 'common\components\ConsoleRunner',
		],
		'authManager' => [
			'class' => 'yii\rbac\PhpManager',
			'itemFile' => '@common/config/rbac/items.php',
			'assignmentFile' => '@common/config/rbac/assignments.php',
			'ruleFile' => '@common/config/rbac/rules.php',
		],
	],
];
