<?php
return [
    'adminEmail' => 'admin@demo-srv.ru',
    'supportEmail' => 'support@demo-srv.ru',
    'user.passwordResetTokenExpire' => 3600,
];
