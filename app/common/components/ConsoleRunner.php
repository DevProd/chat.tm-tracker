<?php
namespace common\components;

class ConsoleRunner extends \yii\base\Component
{
	public $app;
	public $php;

	/**
	 * Запуск консольной комманды в двух режимах Wait/Background
	 * @param $cmd
	 * @param bool $background
	 * @return string
	 */
	public function run($cmd, $background = false)
	{
		$line = '';

		if ($this->isWindows()) {
			if ($background) {
				$line .= 'start ';
				//$line .= '/b ';
			}
		}

		$line .= $this->php . ' ' . $this->app . ' ';
		$line .= $cmd;

		if (!$this->isWindows() && $background) {
			if (!strpos($cmd, '>')) {
				$line .= ' > /dev/null ';
			}
			$line .= ' 2>/dev/null & ';
		}

		$out = array();
		if ($this->isWindows() && $background) {
			$handle = popen($line, 'r');
			if ($handle === FALSE) {
				die("Unable to execute $line");
			}
			pclose($handle);
		}
		else {
			exec($line, $out);
		}
		return implode(PHP_EOL, $out).PHP_EOL;
	}

	/**
	 * Function to check operating system
	 */
	public function isWindows()
	{
		if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN')
			return true;
		else
			return false;
	}
}
