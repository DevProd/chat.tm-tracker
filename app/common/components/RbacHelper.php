<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.10.2014
 * Time: 22:18
 */

namespace common\components;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

class RbacHelper
{

    /**
     * @var string
     */
    static $ruleClassName = 'frontend\filters\AccessRule';

    /**
     * @return array
     */
    static function getTasks()
    {
        $tasks = [];
        foreach (static::getControllersMap() as $controllerId => $actions) {
            $permissions = [];
            foreach ($actions as $actionName) {
                $permissions[] = str_replace('action-', '', strtolower(
                    preg_replace('/([^A-Z]{1})([A-Z])/', '$1-$2', lcfirst($actionName))
                ));
            }
            $tasks[$controllerId] = $permissions;
        };

        return $tasks;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    static function getControllersMap()
    {
        $controllersMap = [];
        $controllersPath = Yii::getAlias('@frontend/controllers/');

        foreach (glob($controllersPath . "*.php") as $path) {
            $controllerId = strtolower(preg_replace(
                '/([^A-Z]{1})([A-Z])/', '$1-$2',
                str_replace([$controllersPath, 'Controller.php'], '', $path)
            ));
            $controller = Yii::$app->createControllerByID($controllerId);
            $behaviors = $controller->behaviors();

            if (isset($behaviors['access'], $behaviors['access']['rules'])) {
                $rules = array_filter($behaviors['access']['rules'], function ($val) {
                    return isset($val['class']) && $val['class'] == static::$ruleClassName;
                });

                if (sizeof($rules)) {
                    $actions = array_filter(
                        get_class_methods($controller),
                        function ($val) {
                            preg_match('/^action([A-Z]+.*)/', $val, $matches);
                            return !empty($matches[1]);
                        }
                    );
                    $controllersMap[$controllerId] = $actions;
                }
            }
        }

        return $controllersMap;
    }

    /**
     * @param $permissionName
     * @param $model ActiveRecord
     * @param $attribute
     * @return bool
     */
    public static function can($permissionName, $model, $attribute)
    {
        return Yii::$app->getUser()->can($permissionName, ['model' => $model, 'attribute' => $attribute]);
    }
} 