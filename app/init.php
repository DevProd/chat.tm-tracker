<?php
include_once(__DIR__ . '/vendor/yiisoft/yii2/helpers/BaseVarDumper.php');
include_once(__DIR__ . '/vendor/yiisoft/yii2/helpers/VarDumper.php');

use yii\helpers\VarDumper;

$dbDriver = 'mysql';

$configFiles = [
	'common/config/main-local.php' => [
		'components' => [
			'db' => $dbDriver == 'mysql'
				? [
					'class' => 'yii\db\Connection',
					'dsn' => 'mysql:host=localhost;dbname=realty-system',
					'username' => 'root',
					'password' => '',
					'charset' => 'utf8',

				]
				: [
					'class' => 'yii\db\Connection',
					'dsn' => 'pgsql:host=localhost;port=5432;dbname=realty-system',
					'username' => 'postgres',
					'password' => 'postgres',
					'charset' => 'utf8',
				],
			'mailer' => [
				'class' => 'yii\swiftmailer\Mailer',
				'viewPath' => '@common/mail',
				'useFileTransport' => true,
			],
			'authManager' => [
				//'class' => 'yii\rbac\DbManager',
				'class' => 'yii\rbac\PhpManager',
				'itemFile' => '@frontend/rbac/items.php',
				'assignmentFile' => '@frontend/rbac/assignments.php',
				'ruleFile' => '@frontend/rbac/rules.php',
			],
		]
	],
	'common/config/params-local.php' => [],
	'console/config/main-local.php' => [],
	'console/config/params-local.php' => [],
	'frontend/config/main-local.php' => [
		'components' => [
			'request' => [
				// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
				'cookieValidationKey' => strtr(substr(base64_encode(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM)), 0, 32), '+/=', '_-.'),
			],
		],
		'bootstrap' => ['debug', 'gii'],
		'modules' => [
			'debug' => 'yii\debug\Module',
			'gii' => 'yii\gii\Module'
		]
	],
	'frontend/config/params-local.php' => []
];
foreach ($configFiles as $file => $data) {
	if (true || !is_file($file)) {
		echo 'Write to file: ' . $file . PHP_EOL;
		file_put_contents($file, "<?php\nreturn " . VarDumper::export($data) . ";\n", LOCK_EX);
	}
}
