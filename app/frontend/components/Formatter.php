<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.10.2014
 * Time: 11:17
 */

namespace frontend\components;


use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Formatter extends \yii\i18n\Formatter
{

	public function asRelation($label, $options)
	{
		if (empty($label)) {
			return $this->nullDisplay;
		}
		$model = ArrayHelper::getValue($options, 'model', null);
		$link = ArrayHelper::getValue($options, 'link', null);
		$attributeLabel = ArrayHelper::getValue($options, 'attributeLabel', null);
		$attributeId = ArrayHelper::getValue($options, 'attributeId', null);

		if ($model == null) {
			throw new InvalidParamException('The $model must instance of ActiveRecord.');
		}
		if ($link == null) {
			throw new InvalidParamException('The $link must not empty.');
		}
		if ($attributeLabel == null) {
			throw new InvalidParamException('The $attributeLabel must not empty.');
		}
		if ($attributeId == null) {
			throw new InvalidParamException('The $attributeId must not empty.');
		}

		$relation = $model->{$link} ? $model->{$link} : null;
		if (null == $relation) {
			return $this->nullDisplay;
		} else {
			$label = $relation->{$attributeLabel};
		}

		$url = ArrayHelper::getValue($options, 'url', '#');
		if (is_callable($url)) {
			$url = $url($model, $attributeId);
		}

		unset($options['model'], $options['link'], $options['attributeLabel'], $options['attributeId'], $options['url']);
		return Html::a($label, $url, $options);
	}
} 