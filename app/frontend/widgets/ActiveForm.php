<?php
namespace frontend\widgets;

use dosamigos\datepicker\DatePicker;
use dosamigos\multiselect\MultiSelect;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class ActiveForm extends \yii\widgets\ActiveForm
{
	/**
	 * @var string
	 */
	static $template;

	/**
	 * @param static | ActiveForm $form
	 * @param ActiveRecord $model
	 * @param string $template
	 * @param string $prefix
	 * @return string
	 */
	static function fields($form, $model, $template = null, $prefix = '')
	{
		if (null === $template) {
			$template = static::$template;
		}
		return strtr(
			$template,
			static::getFields($form, $model, $prefix)
		);
	}

	static function getFields($form, $model, $prefix = '')
	{
		return [];
	}

	/**
	 * @param static | ActiveForm $form
	 * @param ActiveRecord $model
	 * @param array $attribute
	 * @param string $prefix
	 * @return \yii\widgets\ActiveField
	 */
	static function fieldPk($form, $model, $attribute, $prefix = '')
	{
		return $form->field($model, $prefix . $attribute)->textInput([
			'readonly' => true
		]);
	}

	/**
	 * @param static | ActiveForm $form
	 * @param ActiveRecord $model
	 * @param array $attribute
	 * @param string $prefix
	 * @param string | ActiveRecord $relation
	 * @param string $attrLabel
	 * @return \yii\widgets\ActiveField
	 */
	static function fieldFk($form, $model, $attribute, $prefix = '', $relation, $attrLabel)
	{
		return $form->field($model, $prefix . $attribute)->dropDownList(
			ArrayHelper::map($relation::find()->all(), 'id', $attrLabel)
		);
	}

	/**
	 * @param static | ActiveForm $form
	 * @param ActiveRecord $model
	 * @param array $attribute
	 * @param string $prefix
	 * @return \yii\widgets\ActiveField
	 */
	static function fieldString($form, $model, $attribute, $prefix = '')
	{
		return $form->field($model, $prefix . $attribute);
	}

	/**
	 * @param static | ActiveForm $form
	 * @param ActiveRecord $model
	 * @param array $attribute
	 * @param string $prefix
	 * @return \yii\widgets\ActiveField
	 */
	static function fieldText($form, $model, $attribute, $prefix = '')
	{
		return $form->field($model, $prefix . $attribute)->textarea();
	}

	/**
	 * @param static | ActiveForm $form
	 * @param ActiveRecord $model
	 * @param array $attribute
	 * @param string $prefix
	 * @return \yii\widgets\ActiveField
	 */
	static function fieldInteger($form, $model, $attribute, $prefix = '')
	{
		return $form->field($model, $prefix . $attribute);
	}

	/**
	 * @param static | ActiveForm $form
	 * @param ActiveRecord $model
	 * @param array $attribute
	 * @param string $prefix
	 * @return \yii\widgets\ActiveField
	 */
	static function fieldBool($form, $model, $attribute, $prefix = '')
	{
		return $form->field($model, $prefix . $attribute)->checkbox();
	}

	/**
	 * @param static | ActiveForm $form
	 * @param ActiveRecord $model
	 * @param array $attribute
	 * @param string $prefix
	 * @param array $items
	 * @return \yii\widgets\ActiveField
	 */
	static function fieldDropDownList($form, $model, $attribute, $prefix = '', $items=array())
	{
		return $form->field($model, $prefix . $attribute)->dropDownList($items);
	}

	/**
	 * @param static | ActiveForm $form
	 * @param ActiveRecord $model
	 * @param array $attribute
	 * @param string $prefix
	 * @return string
	 */
	static function fieldDate($form, $model, $attribute, $prefix = '')
	{
		$field = $form->field($model, $prefix . $attribute);
		$value = $model->{$attribute};
		$field->parts['{input}'] = DatePicker::widget([
			'model' => $model,
			'name' => Html::getInputName($model, $prefix . $attribute),
			'value' =>  $value ? \Yii::$app->getFormatter()->format($value, 'date') : null,
			'template' => '{input}{addon}',
			'id' => Html::getInputId($model, $prefix . $attribute),
			'clientOptions' => [
				'autoclose' => true,
				'format' => 'dd.mm.yyyy'
			]
		]);
		return $field;
	}

	/**
	 * @param static | ActiveForm $form
	 * @param ActiveRecord $model
	 * @param array $attribute
	 * @param string $prefix
	 * @return string
	 */
	static function fieldTimestamp($form, $model, $attribute, $prefix = '')
	{
		$field = $form->field($model, $prefix . $attribute);
		$value = $model->{$attribute};
		$field->parts['{input}'] = DatePicker::widget([
			'model' => $model,
			'name' => Html::getInputName($model, $prefix . $attribute),
			'value' =>  $value ? \Yii::$app->getFormatter()->format($value, 'date') : null,
			'template' => '{input}{addon}',
			'id' => Html::getInputId($model, $prefix . $attribute),
			'clientOptions' => [
				'autoclose' => true,
				'format' => 'php:d.m.Y H:i:s'
			]
		]);
		return $field;
	}

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord $model
	 * @param string $prefix
	 * @return string|\yii\widgets\ActiveField
	 */
	static function getFieldMultiSelect($form, $model, $attribute, $data, $prefix = '')
	{
		if (empty($data)) {
			$data = ['' => 'Даннные не найдены'];
		}
		$field = $form->field($model, $prefix . $attribute);
		$field->parts['{input}'] = MultiSelect::widget([
			'data' => $data,
			'model' => $model,
			'attribute' => $attribute,
			'options' => [
				'multiple' => 'multiple',
			],
			'clientOptions' => [
				'buttonContainer' => '<div class="btn-group" style="width: 100%; text-align: left"/>',
				'buttonWidth' => '100%',
				'includeSelectAllOption' => true,
				'enableFiltering' => true,
				'numberDisplayed' => 10
			]
		]);
		return $field;
	}
}
