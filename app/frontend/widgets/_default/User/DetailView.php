<?php
namespace frontend\widgets\_default\User;

use yii\helpers\Url;

/**
* Class DetailView
*/
class DetailView extends \frontend\widgets\DetailView
{
	/**
	 * @return array
	 */
	public function defaultAttributes()
	{
		return [
				'id' => [
					'attribute' => 'id',
					'format' => [
						'text'
					],
				],
				'username' => [
					'attribute' => 'username',
					'format' => [
						'text'
					],
				],
				'email' => [
					'attribute' => 'email',
					'format' => [
						'text'
					],
				],
				'password_hash' => [
					'attribute' => 'password_hash',
					'format' => [
						'text'
					],
				],
				'password_reset_token' => [
					'attribute' => 'password_reset_token',
					'format' => [
						'text'
					],
				],
				'auth_key' => [
					'attribute' => 'auth_key',
					'format' => [
						'text'
					],
				],
				'status' => [
					'attribute' => 'status',
					'format' => [
						'text'
					],
				],
				'updated_at' => [
					'attribute' => 'updated_at',
					'format' => [
						'datetime'
					],
				],
				'created_at' => [
					'attribute' => 'created_at',
					'format' => [
						'datetime'
					],
				],
		];
	}
}