<?php
namespace frontend\widgets\_default\User;

use common\models\User;
use yii\db\ActiveRecord;

/**
 * Class ActiveForm
 */
class ActiveForm extends \frontend\widgets\ActiveForm
{
	/**
	 * @var string
	 */
	static $template = '
		{id}
		{username}
		{email}
		{password_hash}
		{password_reset_token}
		{auth_key}
		{status}
		{updated_at}
		{created_at}
	';

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | User $model
	 * @param string $prefix
	 * @return array
	 */
	static function getFields($form, $model, $prefix = '')
	{
		return [
			'{id}' => static::getFieldId($form, $model, $prefix),
			'{username}' => static::getFieldUsername($form, $model, $prefix),
			'{email}' => static::getFieldEmail($form, $model, $prefix),
			'{password_hash}' => static::getFieldPasswordHash($form, $model, $prefix),
			'{password_reset_token}' => static::getFieldPasswordResetToken($form, $model, $prefix),
			'{auth_key}' => static::getFieldAuthKey($form, $model, $prefix),
			'{status}' => static::getFieldStatus($form, $model, $prefix),
			'{updated_at}' => static::getFieldUpdatedAt($form, $model, $prefix),
			'{created_at}' => static::getFieldCreatedAt($form, $model, $prefix),
		];
	}

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | User $model
	 * @param string $prefix
	 * @return string|\yii\widgets\ActiveField
	 */
	static function getFieldId($form, $model, $prefix = '')
	{
		return static::fieldPk($form, $model, 'id', $prefix);
	}

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | User $model
	 * @param string $prefix
	 * @return string|\yii\widgets\ActiveField
	 */
	static function getFieldUsername($form, $model, $prefix = '')
	{
		return static::fieldString($form, $model, 'username', $prefix);
	}

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | User $model
	 * @param string $prefix
	 * @return string|\yii\widgets\ActiveField
	 */
	static function getFieldEmail($form, $model, $prefix = '')
	{
		return static::fieldString($form, $model, 'email', $prefix);
	}

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | User $model
	 * @param string $prefix
	 * @return string|\yii\widgets\ActiveField
	 */
	static function getFieldPasswordHash($form, $model, $prefix = '')
	{
		return static::fieldString($form, $model, 'password_hash', $prefix);
	}

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | User $model
	 * @param string $prefix
	 * @return string|\yii\widgets\ActiveField
	 */
	static function getFieldPasswordResetToken($form, $model, $prefix = '')
	{
		return static::fieldString($form, $model, 'password_reset_token', $prefix);
	}

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | User $model
	 * @param string $prefix
	 * @return string|\yii\widgets\ActiveField
	 */
	static function getFieldAuthKey($form, $model, $prefix = '')
	{
		return static::fieldString($form, $model, 'auth_key', $prefix);
	}

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | User $model
	 * @param string $prefix
	 * @return string|\yii\widgets\ActiveField
	 */
	static function getFieldStatus($form, $model, $prefix = '')
	{
		return static::fieldInteger($form, $model, 'status', $prefix);
	}

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | User $model
	 * @param string $prefix
	 * @return string|\yii\widgets\ActiveField
	 */
	static function getFieldUpdatedAt($form, $model, $prefix = '')
	{
		return static::fieldTimestamp($form, $model, 'updated_at', $prefix);
	}

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | User $model
	 * @param string $prefix
	 * @return string|\yii\widgets\ActiveField
	 */
	static function getFieldCreatedAt($form, $model, $prefix = '')
	{
		return static::fieldTimestamp($form, $model, 'created_at', $prefix);
	}
}