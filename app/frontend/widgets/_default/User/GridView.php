<?php
namespace frontend\widgets\_default\User;

use yii\helpers\Url;

/**
* Class GridView
*/
class GridView extends \frontend\widgets\GridView
{
	/**
	 * @var string
	 */
	public $model = 'common\models\User';

	/**
	 * @var string
	 */
	public $templateItems = '<table{attrItems}">
		<thead>
			{caption}
			<tr{attrItemHeader}>
				<th>{header_id}</th>
				<th>{header_username}</th>
				<th>{header_email}</th>
				<th>{header_password_hash}</th>
				<th>{header_password_reset_token}</th>
				<th>{header_auth_key}</th>
				<th>{header_status}</th>
				<th>{header_updated_at}</th>
				<th>{header_created_at}</th>
				<th><div style="width:50px">{header__actions}</div></th>
			</tr>
			<tr>
				{filter_id}
				{filter_username}
				{filter_email}
				{filter_password_hash}
				{filter_password_reset_token}
				{filter_auth_key}
				{filter_status}
				{filter_updated_at}
				{filter_created_at}
				{filter__actions}
			</tr>
		</thead>
		<tbody>{body}</tbody>
	</table>';

	/**
	 * @var string
	 */
	public $templateBodyRow = '<tr{attrItemBody}>
		<td>{id}</td>
		<td>{username}</td>
		<td>{email}</td>
		<td>{password_hash}</td>
		<td>{password_reset_token}</td>
		<td>{auth_key}</td>
		<td>{status}</td>
		<td>{updated_at}</td>
		<td>{created_at}</td>
		{_actions}
	</tr>';

	/**
	 * @return array
	 */
	static function defaultColumns()
	{
		return [
			'id' => [
				'attribute' => 'id',
				'format' => [
					'text'
				],
			],
			'username' => [
				'attribute' => 'username',
				'format' => [
					'text'
				],
			],
			'email' => [
				'attribute' => 'email',
				'format' => [
					'text'
				],
			],
			'password_hash' => [
				'attribute' => 'password_hash',
				'format' => [
					'text'
				],
			],
			'password_reset_token' => [
				'attribute' => 'password_reset_token',
				'format' => [
					'text'
				],
			],
			'auth_key' => [
				'attribute' => 'auth_key',
				'format' => [
					'text'
				],
			],
			'status' => [
				'attribute' => 'status',
				'format' => [
					'text'
				],
			],
			'updated_at' => [
				'attribute' => 'updated_at',
				'format' => [
					'datetime'
				],
			],
			'created_at' => [
				'attribute' => 'created_at',
				'format' => [
					'datetime'
				],
			],
			'_actions' => [
				'class' => 'yii\grid\ActionColumn',
				'header' => '&#160;',
			]
		];
	}
}
