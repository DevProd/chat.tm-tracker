<?php
namespace frontend\widgets\Chat;

use common\models\Chat;
use frontend\widgets\ActiveForm;
use yii\db\ActiveRecord;

/**
 * Class ActiveForm
 */
class SaveForm extends ActiveForm
{
	/**
	 * @var string
	 */
	static $template = '
    <div class="row">
		<div class="col-md-12">{name}</div>
        <div class="col-md-12">{description}</div>
        <div class="col-md-12">{enable_repository_events}</div>
    </div>
	';

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | Chat $model
	 * @param string $prefix
	 * @return array
	 */
	static function getFields($form, $model, $prefix = '')
	{
		return [
			'{name}' => static::fieldString($form, $model, 'name', $prefix),
            '{description}' => static::fieldText($form, $model, 'description', $prefix),
            '{enable_repository_events}' => static::fieldBool($form, $model, 'enable_repository_events', $prefix),
		];
	}
}