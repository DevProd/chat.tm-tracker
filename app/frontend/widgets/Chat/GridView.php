<?php
namespace frontend\widgets\Chat;

use common\components\RbacHelper;
use common\models\Chat;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridView
 */
class GridView extends \frontend\widgets\GridView
{
    /**
     * @var string
     */
    public $model = 'common\models\Chat';

    public $layout = "{items}\n{pager}";

    /**
     * @var string
     */
    public $templateItems = '<table{attrItems}">
		<thead>
			{caption}
			<tr{attrItemHeader}>
				<th>{header_owner_id}</th>
				<th>{header_name}</th>
				<th>{header_enable_repository_events}</th>
				<th>{header_created_at}</th>
				<th><div style="width:10px">{header__actions}</div></th>
			</tr>
		</thead>
		<tbody>{body}</tbody>
	</table>';

    /**
     * @var string
     */
    public $templateBodyRow = '<tr{attrItemBody}>
		<td>{owner_id}</td>
		<td>{name}</td>
		<td>{enable_repository_events}</td>
		<td>{created_at}</td>
		{_actions}
	</tr>';

    /**
     * @return array
     */
    static function defaultColumns()
    {
        return [
            'owner_id' => [
                'attribute' => 'owner_id',
                'format' => [
                    'text'
                ],
                'value' => function ($model) {
                    /** @var $model Chat */
                    $owner = $model->getOwner();
                    return $owner ? $owner->username : null;
                }
            ],
            'name' => [
                'attribute' => 'name',
                'format' => [
                    'text'
                ],
            ],
            'enable_repository_events' => [
                'attribute' => 'enable_repository_events',
                'format' => [
                    'boolean'
                ],
            ],
            'created_at' => [
                'attribute' => 'created_at',
                'format' => [
                    'date'
                ],
            ],
            '_actions' => [
                'class' => 'yii\grid\ActionColumn',
                'header' => '&#160;',
                'template' => '{chat} {users}',
                'contentOptions' => ['style' => 'text-align: center;'],
                'buttons' => [
                    'chat' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-play"></span>', [
                            '/chat',
                            'id' => $model->id
                        ], [
                            'title' => \Yii::t('system', 'Open chat'),
                            'data-pjax' => '0',
                        ]);
                    },
                    'users' => function ($url, $model) {
                        return RbacHelper::can('chat/manage-access', $model, 'owner_id')
                            ? Html::a('<span class="glyphicon glyphicon-user"></span>', 'javascript://', [
                                'class' => 'chat-manage-access-js',
                                'title' => \Yii::t('system', 'Manage user access'),
                                'data-action-url' => Url::to(['/chat/manage-access', 'id' => $model->id]),
                                'data-header-text' => $model->name,
                                'data-toggle' => 'modal',
                                'data-target' => '#chat-manage-access',
                            ])
                            : null;
                    }
                ]
            ]
        ];
    }
}
