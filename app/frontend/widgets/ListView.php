<?php
namespace frontend\widgets;

use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;

class ListView extends \yii\widgets\ListView
{
	/**
	 * @var ActiveRecord
	 */
	public $model;

	/**
	 * @var ActiveQuery
	 */
	public $query;

	/**
	 * Initializes the grid view.
	 * This method will initialize required property values and instantiate [[columns]] objects.
	 */
	public function init()
	{
		if (!$this->dataProvider) {
			$this->dataProvider = new ActiveDataProvider([
				'query' => $this->query ? $this->query : call_user_func(array($this->model, 'find')),
			]);
		}

		if (!$this->itemView) {
			$this->itemView = array($this, 'itemView');
		}

		parent::init();
	}

	/**
	 * @param ActiveRecord $model
	 * @param integer $key
	 * @param integer $index
	 * @param ListView $widget
	 * @return string
	 */
	public function itemView($model, $key, $index, $widget)
	{
		/** @var DetailView $widget */
		$widget = \Yii::createObject([
			'class' => str_replace('ListView', 'DetailView', get_called_class()),
			'model' => $model,
		]);
		$attributes = $widget->defaultAttributes();
		$attributesList = array_chunk($attributes, ceil(sizeof($attributes) / 2), true);

		$pairs = [];
		foreach ($attributesList as $i=>$attributes) {
			$pairs['{column'.$i.'}'] = DetailView::widget([
				'model' => $model,
				'attributes' => $attributes
			]);
		}

		return strtr(
			'<div class="row">
				<div class="col-md-6">{column0}</div>
				<div class="col-md-6">{column1}</div>
				<div class="col-md-12">
				'.Html::a(\Yii::t('system', 'Просмотреть'), Url::toRoute(['view', 'id' => $model->id]), ['class' => 'btn btn-success']).'
				</div>
			</div>',
			$pairs
		);
	}
}
