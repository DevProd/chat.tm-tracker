<?php
namespace frontend\widgets\Repository;

use common\models\Repository;
use frontend\widgets\ActiveForm;
use yii\db\ActiveRecord;

/**
 * Class ActiveForm
 */
class SaveForm extends ActiveForm
{
	/**
	 * @var string
	 */
	static $template = '
    <div class="row">
		<div class="col-md-12">{name}</div>
        <div class="col-md-12">{alias}</div>
        <div class="col-md-12">{repository_url}</div>
        <div class="col-md-12">{type}</div>
        <div class="col-md-12">{description}</div>
    </div>
	';

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | Repository $model
	 * @param string $prefix
	 * @return array
	 */
	static function getFields($form, $model, $prefix = '')
	{
		return [
			//'{uuid}' => static::fieldString($form, $model, 'uuid', $prefix),
			'{name}' => static::fieldString($form, $model, 'name', $prefix),
            '{alias}' => static::fieldString($form, $model, 'alias', $prefix),
            '{repository_url}' => static::fieldString($form, $model, 'repository_url', $prefix),
            '{type}' => static::fieldDropDownList($form, $model, 'type', $prefix, Repository::getTypes()),
            '{description}' => static::fieldString($form, $model, 'description', $prefix)->textarea(),
		];
	}
}