<?php
namespace frontend\widgets\Repository;

use common\models\Project;
use common\models\Repository;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridView
 */
class GridView extends \frontend\widgets\GridView
{
    /**
     * @var string
     */
    public $model = 'common\models\Repository';

    public $layout = "{items}\n{pager}";

    /**
     * @var string
     */
    public $templateItems = '<table{attrItems}">
		<thead>
			{caption}
			<tr{attrItemHeader}>
                <th>{header_name}</th>
	            <th>{header_owner_id}</th>
				<th>{header_alias}</th>
				<th>{header_status}</th>
				<th>{header_created_at}</th>
				<th><div style="width:20px">{header__actions}</div></th>
			</tr>
		</thead>
		<tbody>{body}</tbody>
	</table>';

    /**
     * @var string
     */
    public $templateBodyRow = '<tr{attrItemBody}>
        <td>{name}</td>
        <td>{owner_id}</td>
		<td>{alias}</td>
		<td>{status}</td>
		<td>{created_at}</td>
		{_actions}
	</tr>';

    /**
     * @return array
     */
    static function defaultColumns()
    {
        return [
            'owner_id' => [
                'attribute' => 'owner_id',
                'format' => [
                    'text'
                ],
                'value' => function ($model) {
                    /** @var $model Repository */
                    $owner = $model->getOwner();
                    return $owner ? $owner->username : null;
                }
            ],
            'project_id' => [
                'attribute' => 'project_id',
                'format' => [
                    'text'
                ],
                'value' => function ($model) {
                    /** @var $model Repository */
                    $project = $model->getProject();
                    return $project ? $project->name : null;
                }
            ],
            'name' => [
                'attribute' => 'name',
                'format' => [
                    'text'
                ],
            ],
            'alias' => [
                'attribute' => 'alias',
                'format' => [
                    'html'
                ],
                'value' => function ($model) {
                    /** @var $model Repository */
                    return Html::a($model->alias, $model->repository_url, ['target' => '_blank']);
                }
            ],
            'status' => [
                'attribute' => 'status',
                'filter' => Project::getStatuses(),
                'format' => [
                    'text'
                ],
                'value' => function ($model) {
                    /** @var $model Repository */
                    return Project::getStatuses()[$model->status];
                }
            ],
            'created_at' => [
                'attribute' => 'created_at',
                'format' => [
                    'date'
                ],
            ],
            '_actions' => [
                'class' => 'yii\grid\ActionColumn',
                'header' => '&#160;',
                'template' => '{view} {edit}',
                'contentOptions' => ['style' => 'text-align: center;'],
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', [
                            '/repository/view',
                            'id' => $model->id
                        ], [
                            'title' => \Yii::t('system', 'Открыть чат проекта'),
                            'data-pjax' => '0',
                        ]);
                    },
                    'edit' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-edit"></span>', [
                            '/repository/edit',
                            'id' => $model->id
                        ], [
                            'title' => \Yii::t('system', 'Открыть чат проекта'),
                            'data-pjax' => '0',
                        ]);
                    }
                ]
            ]
        ];
    }
}
