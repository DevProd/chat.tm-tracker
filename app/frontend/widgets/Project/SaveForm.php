<?php
namespace frontend\widgets\Project;

use common\models\Project;
use frontend\widgets\ActiveForm;
use yii\db\ActiveRecord;

/**
 * Class ActiveForm
 */
class SaveForm extends ActiveForm
{
	/**
	 * @var string
	 */
	static $template = '
    <div class="row">
		<div class="col-md-12">{name}</div>
		<div class="col-md-12">{description}</div>
    </div>
	';

	/**
	 * @param \frontend\widgets\ActiveForm | ActiveForm $form
	 * @param ActiveRecord | Project $model
	 * @param string $prefix
	 * @return array
	 */
	static function getFields($form, $model, $prefix = '')
	{
		return [
			'{name}' => static::fieldString($form, $model, 'name', $prefix),
			'{description}' => static::fieldString($form, $model, 'description', $prefix)->textarea(),
		];
	}
}