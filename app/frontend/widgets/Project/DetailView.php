<?php
namespace frontend\widgets\Project;

use common\models\Project;
use common\models\User;
use yii\helpers\Html;

/**
* Class DetailView
*/
class DetailView extends \frontend\widgets\DetailView
{
	/**
	 * @return array
	 */
	public function defaultAttributes()
	{
		/* @var $model Project */

		return [
				'owner_id' => [
					'attribute' => 'owner_id',
					'format' => [
						'html'
					],
                    'value' => ($user = $this->model->getOwner())
                        ? Html::a($user->username, ['user/view', 'id' => $user->id]) . ' (' . \Yii::t('roles', User::getRoles($user->role)) . ')'
                        : null
				],
				'name' => [
					'attribute' => 'name',
					'format' => [
						'text'
					],
				],
				'status' => [
					'attribute' => 'status',
					'format' => [
						'text'
					],
					'value' => $this->model->status ? Project::getStatuses()[$this->model->status] : null

				],
				'updated_at' => [
					'attribute' => 'updated_at',
					'format' => [
						'datetime'
					],
				],
				'created_at' => [
					'attribute' => 'created_at',
					'format' => [
						'datetime'
					],
				],
		];
	}
}