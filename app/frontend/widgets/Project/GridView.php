<?php
namespace frontend\widgets\Project;

use common\models\Project;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridView
 */
class GridView extends \frontend\widgets\GridView
{
    /**
     * @var string
     */
    public $model = 'common\models\Project';

    /**
     * @var string
     */
    public $templateItems = '<table{attrItems}">
		<thead>
			{caption}
			<tr{attrItemHeader}>
				<th>{header_owner_id}</th>
				<th>{header_name}</th>
				<th>{header_status}</th>
				<th>{header_created_at}</th>
				<th><div style="width:50px">{header__actions}</div></th>
			</tr>
			<tr{attrFilter}>
				{filter_owner_id}
				{filter_name}
				{filter_status}
				{filter_created_at}
				{filter__actions}
			</tr>
		</thead>
		<tbody>{body}</tbody>
	</table>';

    /**
     * @var string
     */
    public $templateBodyRow = '<tr{attrItemBody}>
		<td>{owner_id}</td>
		<td>{name}</td>
		<td>{status}</td>
		<td>{created_at}</td>
		{_actions}
	</tr>';

    /**
     * @return array
     */
    static function defaultColumns()
    {
        return [
            'owner_id' => [
                'attribute' => 'owner_id',
                'format' => [
                    'text'
                ],
            ],
            'name' => [
                'attribute' => 'name',
                'format' => [
                    'text'
                ],
            ],
            'status' => [
                'attribute' => 'status',
                'filter' => Project::getStatuses(),
                'format' => [
                    'text'
                ],
                'value' => function ($model, $key, $index, $column) {
                    /**
                     * @var $model Project
                     * @var $key integer
                     * @var $index integer
                     * @var $column DataColumn
                     */
                    return Project::getStatuses()[$model->status];
                }
            ],
            'created_at' => [
                'attribute' => 'created_at',
                'format' => [
                    'datetime'
                ],
            ],
            '_actions' => [
                'class' => 'yii\grid\ActionColumn',
                'header' => '&#160;',
                'contentOptions' => [
                    'style' => 'text-align: right; width: 70px;'
                ],
                'template' => '{update}{view}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return \Yii::$app->getUser()->can('project/update', ['model' => $model, 'attribute' => 'owner_id'])
                            ? Html::a('<span class="glyphicon glyphicon-play"></span>', [
                                '/project/update',
                                'id' => $model->id
                            ], [
                                'title' => \Yii::t('project', 'Edit project'),
                                'style' => 'margin-right: 5px',
                                'data-pjax' => '0',
                            ])
                            : null;
                    }
                ]
            ]
        ];
    }
}
