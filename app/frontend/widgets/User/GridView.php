<?php
namespace frontend\widgets\User;

use common\models\User;
use common\models\UserSettings;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridView
 */
class GridView extends \frontend\widgets\_default\User\GridView
{
	/**
	 * @var string
	 */
	public $model = 'common\models\User';

	/**
	 * @var string
	 */
	public $templateItems = '<table{attrItems}">
		<thead>
			{caption}
			<tr{attrItemHeader}>
				<th>{header_id}</th>
				<th>{header_username}</th>
				<th>{header_email}</th>
				<th>{header_status}</th>
				<th>{header_updated_at}</th>
				<th>{header_created_at}</th>
				<th><div style="width:50px">{header__actions}</div></th>
			</tr>
			<tr{attrFilter}>
				{filter_id}
				{filter_username}
				{filter_email}
				{filter_status}
				{filter_updated_at}
				{filter_created_at}
				{filter__actions}
			</tr>
		</thead>
		<tbody>{body}</tbody>
	</table>';

	/**
	 * @var string
	 */
	public $templateBodyRow = '<tr{attrItemBody}>
		<td>{id}</td>
		<td>{username}</td>
		<td>{email}</td>
		<td>{status}</td>
		<td>{updated_at}</td>
		<td>{created_at}</td>
		{_actions}
	</tr>';

	/**
	 * @return array
	 */
	static function defaultColumns()
	{
		return [
			'id' => [
				'attribute' => 'id',
				'format' => [
					'text'
				],
			],
			'username' => [
				'attribute' => 'username',
				'format' => [
					'text'
				],
			],
			'email' => [
				'attribute' => 'email',
				'format' => [
					'text'
				],
			],
			'password_hash' => [
				'attribute' => 'password_hash',
				'format' => [
					'text'
				],
			],
			'password_reset_token' => [
				'attribute' => 'password_reset_token',
				'format' => [
					'text'
				],
			],
			'auth_key' => [
				'attribute' => 'auth_key',
				'format' => [
					'text'
				],
			],
			'status' => [
				'attribute' => 'status',
				'filter' => User::getStatuses(),
				'format' => [
					'text'
				],
				'value' => function ($model, $key, $index, $column) {
					/**
					 * @var $model User
					 * @var $key integer
					 * @var $index integer
					 * @var $column DataColumn
					 */
					return User::getStatuses()[$model->status];
				}
			],
			'updated_at' => [
				'attribute' => 'updated_at',
				'format' => [
					'datetime'
				],
			],
			'created_at' => [
				'attribute' => 'created_at',
				'format' => [
					'datetime'
				],
			],
			'_actions' => [
				'class' => 'yii\grid\ActionColumn',
				'header' => '&#160;',
				'template' => '{settings} {view} {update} {delete}',
				'buttons' => [
					'settings' => function ($url, $model) {
						return Html::a('<span class="glyphicon glyphicon-cog"></span>', [
							'settings',
							'id' => $model->id
						], [
							'title' => \Yii::t('system', 'Редактирование настроек пользователя'),
							'data-pjax' => '0',
						]);
					}
				]
			]
		];
	}
}
