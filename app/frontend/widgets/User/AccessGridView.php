<?php
namespace frontend\widgets\User;

use common\models\User;
use frontend\widgets\GridView;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * Class AccessGridView
 */
class AccessGridView extends GridView
{
    /**
     * @var string
     */
    public $model = 'common\models\User';

    /**
     * @var string
     */
    public $templateItems = '<table{attrItems}">
		<thead>
			{caption}
			<tr{attrItemHeader}>
				<th>{header_email}</th>
				<th><div style="width:10px">{header__actions}</div></th>
			</tr>
    </thead>
		<tbody>{body}</tbody>
	</table>';

    /**
     * @var string
     */
    public $templateBodyRow = '<tr{attrItemBody}>
		<td>{email}</td>
		{_actions}
	</tr>';

    /**
     * @return array
     */
    static function defaultColumns()
    {
        return [
            'email' => [
                'attribute' => 'email',
                'format' => [
                    'text'
                ],
            ],
            '_actions' => [
                'class' => 'yii\grid\ActionColumn',
                'header' => '&#160;',
                'template' => '{view}',
                'contentOptions' => ['style' => 'text-align: center;'],
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', [
                            '/user/view',
                            'id' => $model['id']
                        ], [
                            'title' => \Yii::t('system', 'User view'),
                            'data-pjax' => '0',
                        ]);
                    }
                ]
            ]
        ];
    }
}
