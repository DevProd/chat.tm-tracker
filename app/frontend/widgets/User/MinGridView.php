<?php
namespace frontend\widgets\User;

use common\models\User;
use frontend\widgets\GridView;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class MinGridView
 */
class MinGridView extends GridView
{
	/**
	 * @var string
	 */
	public $model = 'common\models\User';

	/**
	 * @var string
	 */
	public $templateItems = '<table{attrItems}">
		<thead>
			{caption}
			<tr{attrItemHeader}>
				<th style="text-align: center;">{header_checked}</th>
				<th>{header_username}</th>
				<th>{header_email}</th>
				<th>{header_status}</th>
				<th><div style="width:10px">{header__actions}</div></th>
			</tr>
		</thead>
		<tbody>{body}</tbody>
	</table>';

	/**
	 * @var string
	 */
	public $templateBodyRow = '<tr{attrItemBody}>
		<td style="text-align: center;">{checked}</td>
		<td>{username}</td>
		<td>{email}</td>
		<td>{status}</td>
		{_actions}
	</tr>';

	/**
	 * @return array
	 */
	static function defaultColumns()
	{
		return [
			'checked' => [
				'attribute' => 'checked',
                'header' => Html::checkbox('grand-access-all', false, []),
                'format' => [
					'raw'
				],
                'value' => function ($model, $key, $index, $column) {
                    /**
                     * @var $model User
                     * @var $key integer
                     * @var $index integer
                     * @var $column DataColumn
                     */
                    return Html::checkbox('grand-access-one', $model['checked'], ['data-user-id' => $model['id']]);
                }
            ],
			'username' => [
				'attribute' => 'username',
				'format' => [
					'text'
				],
			],
			'email' => [
				'attribute' => 'email',
				'format' => [
					'text'
				],
			],
			'status' => [
				'attribute' => 'status',
				'filter' => User::getStatuses(),
				'format' => [
					'text'
				],
				'value' => function ($model, $key, $index, $column) {
					/**
					 * @var $model User
					 * @var $key integer
					 * @var $index integer
					 * @var $column DataColumn
					 */
					return User::getStatuses()[$model['status']];
				}
			],
			'_actions' => [
				'class' => 'yii\grid\ActionColumn',
				'header' => '&#160;',
				'template' => '{view}',
                'contentOptions' => ['style' => 'text-align: center;'],
                'buttons' => [
					'view' => function ($url, $model) {
						return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', [
							'/user/view',
							'id' => $model['id']
						], [
							'title' => \Yii::t('system', 'User view'),
							'data-pjax' => '0',
						]);
					}
				]
			]
		];
	}
}
