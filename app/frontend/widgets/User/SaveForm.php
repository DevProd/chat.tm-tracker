<?php
namespace frontend\widgets\User;

use common\models\User;
use frontend\widgets\_default\User\ActiveForm;
use yii\db\ActiveRecord;

/**
 * Class ActiveForm
 */
class SaveForm extends ActiveForm
{
    /**
     * @var string
     */
    static $template = '
		{username}
		{email}
		{role}
		{password_hash}
	';

    /**
     * @param \frontend\widgets\ActiveForm | ActiveForm $form
     * @param ActiveRecord | User $model
     * @param string $prefix
     * @return array
     */
    static function getFields($form, $model, $prefix = '')
    {
        return [
            '{username}' => static::getFieldUsername($form, $model, $prefix),
            '{email}' => static::getFieldEmail($form, $model, $prefix),
            '{role}' => static::getFieldUserRoles($form, $model, $prefix),
            '{password_hash}' => static::getFieldPassword($form, $model, $prefix),
        ];
    }

    /**
     * @param \frontend\widgets\ActiveForm | ActiveForm $form
     * @param ActiveRecord | User $model
     * @param string $prefix
     * @return string|\yii\widgets\ActiveField
     */
    static function getFieldPassword($form, $model, $prefix = '')
    {
        return static::fieldString($form, $model, 'password_hash', $prefix)->passwordInput();
    }


    /**
     * @param \frontend\widgets\ActiveForm | ActiveForm $form
     * @param ActiveRecord | User $model
     * @param string $prefix
     * @return string|\yii\widgets\ActiveField
     */
    static function getFieldStatus($form, $model, $prefix = '')
    {
        return static::fieldDropDownList($form, $model, 'status', $prefix, array('' => '') + User::getStatuses());
    }

    /**
     * @param \frontend\widgets\ActiveForm | ActiveForm $form
     * @param ActiveRecord | User $model
     * @param string $prefix
     * @return string|\yii\widgets\ActiveField
     */
    static function getFieldUserRoles($form, $model, $prefix = '')
    {
        return static::fieldDropDownList($form, $model, 'role', $prefix, User::getRoles());
    }
}