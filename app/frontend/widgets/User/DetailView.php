<?php
namespace frontend\widgets\User;

use yii\helpers\Url;

/**
* Class DetailView
*/
class DetailView extends \frontend\widgets\_default\User\DetailView
{
	/**
	 * @return array
	 */
	public function defaultAttributes()
	{
		return [
				'id' => [
					'attribute' => 'id',
					'format' => [
						'text'
					],
				],
				'username' => [
					'attribute' => 'username',
					'format' => [
						'text'
					],
				],
				'email' => [
					'attribute' => 'email',
					'format' => [
						'text'
					],
				],
				'updated_at' => [
					'attribute' => 'updated_at',
					'format' => [
						'datetime'
					],
				],
				'created_at' => [
					'attribute' => 'created_at',
					'format' => [
						'datetime'
					],
				],
		];
	}
}