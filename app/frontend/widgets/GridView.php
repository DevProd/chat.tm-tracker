<?php
namespace frontend\widgets;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecord;
use yii\grid\Column;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Inflector;

class GridView extends \yii\grid\GridView
{

	/**
	 * @var ActiveRecord
	 */
	public $model;

	/**
	 * @var ActiveQuery
	 */
	public $query;

	/**
	 * @var bool
	 */
	public $filterShow = false;

	public $attrItems = ' class="table table-striped table-bordered"';
	public $attrItemHeader = '';
	public $attrItemBody = ' data-key="{key}"';
	public $attrFilter = ' id="{_id}-filters"';

	/**
	 * @var string the layout that determines how different sections of the list view should be organized.
	 * The following tokens will be replaced with the corresponding section contents:
	 *
	 * - `{summary}`: the summary section. See [[renderSummary()]].
	 * - `{errors}`: the filter model error summary. See [[renderErrors()]].
	 * - `{items}`: the list items. See [[renderItems()]].
	 * - `{sorter}`: the sorter. See [[renderSorter()]].
	 * - `{pager}`: the pager. See [[renderPager()]].
	 */
	public $layout = "{summary}\n{items}\n{pager}";

	/**
	 *
	 * Example:
	 *    <table{attrItems}">
	 *        <thead>
	 *            {caption}
	 *            <colgroup>
	 *                <col/>
	 *                <col/>
	 *            </colgroup>
	 *            <tr{attrItemHeader}>
	 *                <th>{header_id}</th>
	 *                <th>{header_contragent_id}</th>
	 *                <th>{header_already_carried}</th>
	 *                <th>{header_date_certification}</th>
	 *                <th>{header_rival}</th>
	 *                <th>{header_number_of_certified_rm}</th>
	 *                <th>{header_cost_rm}</th>
	 *            </tr>
	 *            {filters}
	 *        </thead>
	 *        <tbody>{body}</tbody>
	 *    </table>
	 *
	 * @var string
	 */
	public $templateItems = '';

	/**
	 * This default template for empty results
	 *
	 * @var string
	 */
	public $templateBodyEmpty = '
		<tr>
			<td colspan="{count}">{content}</td>
		</tr>
	';

	/**
	 *
	 * Example:
	 *    <tr{attrItemBody}>
	 *        <td>{id}</td>
	 *        <td>{contragent_id}</td>
	 *        <td>{already_carried}</td>
	 *        <td>{date_certification}</td>
	 *        <td>{rival}</td>
	 *        <td>{number_of_certified_rm}</td>
	 *        <td>{cost_rm}</td>
	 *    </tr>
	 *
	 * @var string
	 */
	public $templateBodyRow = '';

	/**
	 * Initializes the grid view.
	 * This method will initialize required property values and instantiate [[columns]] objects.
	 */
	public function init()
	{
		if (!$this->dataProvider) {
			$this->dataProvider = new ActiveDataProvider([
				'query' => $this->query ? $this->query : call_user_func(array($this->model, 'find')),
			]);
		}
		parent::init();
	}

	/**
	 * @throws \yii\base\InvalidConfigException
	 */
	protected function initColumns()
	{
		if (!$this->columns) {
			$this->columns = $this->defaultColumns();
		}
		parent::initColumns();
	}

	/**
	 * @return array
	 */
	static function defaultColumns()
	{
		return [];
	}

	/**
	 * Renders the data models for the grid view.
	 */
	public function renderItems()
	{
		return strtr(
			strtr(
				strtr(
					strtr(
						strtr(
							$this->getTemplateItems(),
							[
								'{attrItems}' => is_callable($this->attrItems) ? call_user_func($this->attrItems, $this) : $this->attrItems,
								'{attrItemHeader}' => is_callable($this->attrItemHeader) ? call_user_func($this->attrItemHeader, $this) : $this->attrItemHeader,
							]
						),
						[
							'{caption}' => $this->renderCaption(),
							'{body}' => $this->renderTableBody(),
							'{attrFilter}' => is_callable($this->attrFilter) ? call_user_func($this->attrFilter, $this) : $this->attrFilter,
						]
					),
					$this->getTableHeaderCells()
				),
				$this->getTableFilterCells()
			),
			[
				'{_id}' => $this->getId(),
			]
		);
	}

	/**
	 * Renders the table body.
	 * @return string the rendering result.
	 */
	public function renderTableBody()
	{
		$models = array_values($this->dataProvider->getModels());
		$keys = $this->dataProvider->getKeys();
		$rows = [];
		foreach ($models as $index => $model) {
			$key = $keys[$index];
			$rows[] = strtr(
				strtr(
					strtr(
						$this->getTemplateBodyRow($model, $key, $index),
						[
							'{attrItemBody}' => is_callable($this->attrItemBody) ? call_user_func($this->attrItemBody, $model, $key, $index, $this) : $this->attrItemBody,
						]
					),
					$this->getTableRowCells($model, $key, $index)
				),
				[
					'{odd}' => $index % 2,
					'{key}' => $key,
					'{index}' => $index,
				]
			);
		}

		if (empty($rows)) {
			return strtr($this->templateBodyEmpty, [
				'{count}' => count($this->columns),
				'{content}' => $this->renderEmpty(),
			]);
		} else {
			return implode("\n", $rows);
		}
	}

	public function getTableHeaderCells()
	{
		$cells = [];
		foreach ($this->columns as $name => $column) {
			$content = null;
			if ($column->header !== null || $column->label === null && $column->attribute === null) {
				$content = trim($column->header) !== '' ? $column->header : $column->grid->emptyCell;
			} else {

				$provider = $column->grid->dataProvider;

				if ($column->label === null) {
					if ($provider instanceof ActiveDataProvider && $provider->query instanceof ActiveQueryInterface) {
						/* @var $model Model */
						$model = new $provider->query->modelClass;
						$label = $model->getAttributeLabel($column->attribute);
					} else {
						$models = $provider->getModels();
						if (($model = reset($models)) instanceof Model) {
							/* @var $model Model */
							$label = $model->getAttributeLabel($column->attribute);
						} else {
							$label = Inflector::camel2words($column->attribute);
						}
					}
				} else {
					$label = $column->label;
				}

				if ($column->attribute !== null && $column->enableSorting &&
					($sort = $provider->getSort()) !== false && $sort->hasAttribute($column->attribute)
				) {
					$content = $sort->link($column->attribute, array_merge($column->sortLinkOptions, ['label' => Html::encode($label)]));
				} else {
					$content = Html::encode($label);
				}
			}

			/* @var $column Column */
			$cells['{header_' . $name . '}'] = $content;
		}
		return $cells;
	}

	/**
	 * Renders the filter.
	 * @return array the rendering result.
	 */
	public function getTableFilterCells()
	{
		$cells = [];
		if ($this->filterModel !== null) {
			foreach ($this->columns as $name => $column) {
				/* @var $column DataColumn */
				$cells['{filter_' . $name . '}'] = $column->renderFilterCell();
			}
		}
		return $cells;
	}

	public function getTableRowCells($model, $key, $index)
	{
		$cells = [];
		foreach ($this->columns as $name => $column) {
			$cells['{' . $name . '}'] = $this->renderDataCell($column, $model, $key, $index);
		}
		return $cells;
	}

	/**
	 * @param DataColumn $column
	 * @param AR $model
	 * @param $key
	 * @param $index
	 * @return mixed|string
	 */
	public function renderDataCell($column, $model, $key, $index) {
		if ($column instanceof DataColumn) {
			if ($column->content === null) {
				$format = $column->format;
				if ($format[0] == 'relation') {
					$format[1]['key'] = $key;
					$format[1]['model'] = $model;
					$format[1]['index'] = $index;
				}
				$content = $column->grid->formatter->format($column->getDataCellValue($model, $key, $index), $format);
			} else {
				if ($column->content !== null) {
					$content = call_user_func($column->content, $model, $key, $index, $column);
				} else {
					$content = $column->grid->emptyCell;
				}
			}
		} else {
			$content = $column->renderDataCell($model, $key, $index);
		}
		return $content;
	}


	public function getTemplateItems()
	{
		if (!$this->templateItems) {
			$cells = [];
			foreach ($this->columns as $name => $column) {
				$cells[] = '<th>{header_' . $name . '}</th>';
			}
			$this->templateItems = '<table{attrItems}"><thead><tr{attrItemHeader}>' . implode($cells) . '</tr>{filters}</thead><tbody>{body}</tbody></table>';
		} elseif (is_callable($this->templateItems)) {
			return call_user_func($this->templateBodyRow, $this);
		}
		return $this->templateItems;
	}

	public function getTemplateBodyRow($model, $key, $index)
	{
		if (!$this->templateBodyRow) {
			$cells = [];
			foreach ($this->columns as $name => $column) {
				$cells[] = '<td>{' . $name . '}</td>';
			}
			$this->templateBodyRow = '<tr{attrItemBody}>' . implode($cells) . '</tr>';
		} elseif (is_callable($this->templateBodyRow)) {
			return call_user_func($this->templateBodyRow, $model, $key, $index, $this);
		}
		return $this->templateBodyRow;
	}
}
