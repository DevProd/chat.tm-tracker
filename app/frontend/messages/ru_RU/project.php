<?php
return [
    'User' => 'Пользователь',
    'Name' => 'Наименование',
    'Description' => 'Описание',
    'Status' => 'Статус',
    'Role' => 'Роль',
    'Updated at' => 'Обновлен',
    'Created at' => 'Создан',

    'Edit project' => 'Редактировать проект',
];