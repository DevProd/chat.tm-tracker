<?php
use common\models\Project;
use kartik\tabs\TabsX;

/**
 * @var $this yii\web\View
 * @var $model Project
 */
$this->title = 'Project';

echo TabsX::widget([
    'containerOptions' => [
        'class' => 'tabs-with-large'
    ],
    'position' => TabsX::POS_LEFT,
    'bordered' => true,
    'items' => [
        [
            'label' => Yii::t('headers', 'Project detail'),
            'content' => $this->render('_tabs/_detail', ['model' => $model]),
        ],
        [
            'label' => Yii::t('headers', 'Repository list'),
            'content' => $this->render('_tabs/_repository_list', ['model' => $model]),
        ],
        [
            'label' => Yii::t('headers', 'Chat list'),
            'content' => $this->render('_tabs/_chat_list', ['model' => $model]),
        ],
        [
            'label' => Yii::t('headers', 'Access management'),
            'content' => $this->render('_tabs/_access_management', ['model' => $model]),
            'visible' => Yii::$app->getUser()->can('project/update', ['model' => $model, 'attribute' => 'owner_id']),
            'active' => true
        ]
    ],
]);