<?php
/**
 * @var $this yii\web\View
 * @var $project \common\models\Project
 */

$this->title = Yii::t('project', 'Start your own project');
?>
<div class="Project-index">
    <h1><?= $this->title ?></h1>
    <hr/>
    <?php $form = \frontend\widgets\ActiveForm::begin() ?>

    <?= \frontend\widgets\Project\SaveForm::fields($form, $project) ?>
    <hr/>
    <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

    <?php $form->end() ?>
</div>
