<?php
use yii\widgets\Pjax;
use common\models\Project;
use common\models\User;
use frontend\widgets\User\AccessGridView;
use goodizer\helpers\ModelHelper;

/**
 * @var $this yii\web\View
 * @var $model Project
 */
$this->registerJsFile($this->getAssetManager()->publish(Yii::getAlias('@webroot/js/project/access-management.js'))[1], [
    'depends' => [\frontend\assets\AppAsset::className()]
]);

$user = new User(['scenario' => 'access-management']);
$query = $model->getUsers()->where(['status' => User::STATUS_ACTIVE]);

if ($user->load(Yii::$app->getRequest()->get()) && !empty($user->email)) {
    $query = User::find()->where(['LIKE', 'email', $user->email]);
}

$form = \yii\bootstrap\ActiveForm::begin([
    'id' => 'user-management-form',
    'method' => 'get',
    'action' => \yii\helpers\Url::to(['/project/view', 'id' => $model->id]),
]);
echo $form->field($user, 'email')->label(Yii::t('user', 'Search by email:'));
$form->end();

Pjax::begin(['id' => 'user-management-list']);
echo AccessGridView::widget(ModelHelper::search(new User(['scenario' => 'access-management']), ['query' => $query]) + [
        'attrItemBody' => function (User $u) use($model){
            if($u->getHasProject()->andWhere(['project_id' => $model->id])->one()) {
                return ' class="grid-checked-row"';
            }

            return null;
        }
    ]);
Pjax::end();