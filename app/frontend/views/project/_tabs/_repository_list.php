<?php
use common\models\Project;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model Project
 */
?>
<?= Yii::$app->getUser()->can('repository/create')
    ? (Html::a(Yii::t('system', 'New repository'), ['/repository/create', 'pid' => $model->id], [
            'class' => 'btn btn-success'
        ]) . '<hr/>') : '' ?>
<?= \frontend\widgets\Repository\GridView::widget(
    \common\helpers\ModelHelper::search(new \common\models\Repository(['project_id' => $model->id]))
) ?>