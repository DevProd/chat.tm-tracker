<?php
use common\models\Project;
use frontend\widgets\Project\DetailView;

/**
 * @var $this yii\web\View
 * @var $model Project
 */

echo \yii\bootstrap\ButtonGroup::widget([
    'buttons' => [
        [
            'label' => Yii::t('system', 'Back'),
            'tagName' => 'a',
            'options' => [
                'href' => \yii\helpers\Url::to('index'),
                'class' => 'btn btn-danger'
            ],
        ],
        [
            'label' => Yii::t('system', 'Edit'),
            'tagName' => 'a',
            'options' => [
                'href' => \yii\helpers\Url::to(['update', 'id' => $model->id]),
                'class' => 'btn btn-success'
            ],
            'visible' => Yii::$app->getUser()->can('project/update', ['model' => $model, 'attribute' => 'owner_id']),
        ],
    ]
]);
?>
    <hr/>
<?= DetailView::widget([
    'model' => $model
]) ?>