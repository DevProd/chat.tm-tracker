<?php
use common\models\Project;

/**
 * @var $this yii\web\View
 * @var $model Project
 */

$this->registerJsFile($this->getAssetManager()->publish(Yii::getAlias('@webroot/js/project/modal.js'))[1], [
    'depends' => [\frontend\assets\AppAsset::className()]
]);
?>
<?= Yii::$app->getUser()->can('project/create-chat')
    ? \yii\helpers\Html::button(Yii::t('system', 'Create chat'), [
        'class' => 'btn btn-success',
        'data-toggle' => 'modal',
        'data-target' => '#create-chat-modal',
        'data-action-url' => \yii\helpers\Url::to(['create-chat', 'id' => $model->id]),
    ])
    : '' ?>
    <hr/>

<?php \yii\widgets\Pjax::begin(['id' => 'chat-update']) ?>
<?= \frontend\widgets\Chat\GridView::widget(
    \common\helpers\ModelHelper::search(new \common\models\Chat(['project_id' => $model->id]))
) ?>
<?php \yii\widgets\Pjax::end() ?>

<?php
if (Yii::$app->getUser()->can('project/create-chat')) {
    \yii\bootstrap\Modal::begin([
        'header' => '<h3>New chat for "' . $model->name . '"</h3>',
        'id' => 'create-chat-modal',
    ]);
    \yii\bootstrap\Modal::end();
}

\yii\bootstrap\Modal::begin([
    'header' => '<h3>New chat for "' . $model->name . '"</h3>',
    'id' => 'chat-manage-access',
]);
\yii\bootstrap\Modal::end();
?>