<?php
/**
 * @var $this yii\web\View
 */
\ext\WebSocket\Asset::register($this);

$this->title = 'Projects';
?>
<div class="Project-index">

    <h1>Projects</h1>

    <hr/>

    <?= \yii\helpers\Html::a('New project', '/project/create', ['class' => 'btn btn-primary']) ?>

    <hr/>

    <?= \frontend\widgets\Project\GridView::widget(
        \common\helpers\ModelHelper::search(new \common\models\Project())
    ) ?>

</div>
