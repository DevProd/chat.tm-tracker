<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use \yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $content string
 * @var $user \common\models\User
 */

AppAsset::register($this);

$ctrlName = Yii::$app->controller->id;
$actionName = Yii::$app->controller->action->id;

$user = Yii::$app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="wrapper" class="content-wrapper">
    <?php
    NavBar::begin(['brandLabel' => 'New project']);
    echo Nav::widget([
        'items' => [
            ['label' => 'Projects', 'url' => ['/project']],
            ['label' => 'Hook Test', 'url' => ['/repository/hook']],
        ],
        'options' => ['class' => 'navbar-nav'],
    ]);
    echo Nav::widget([
        'items' => Yii::$app->user->isGuest
            ? [
                ['label' => 'Registration', 'url' => ['/site/sign-up']],
                ['label' => 'Login', 'url' => ['/site/login']],
            ]
            : [
                [
                    'label' => 'RBAC',
                    'url' => '/rbac',
                    'visible' => $user->role == \common\models\User::ROLE_ADMIN,
                ],
                ['label' => 'Logout(' . $user->email . ')', 'url' => ['/site/logout']],
            ],
        'options' => ['class' => 'navbar-nav pull-right'],
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Alert::widget() ?>
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>
<div class="footer-wrapper">
    <div class="footer">
        <p class="pull-left">&copy; Союз <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
