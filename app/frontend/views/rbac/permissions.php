<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.10.2014
 * Time: 10:59
 */
/**
 * @var $tasks array
 * @var $roles array
 * @var $auth \yii\rbac\PhpManager
 */

$auth = Yii::$app->authManager;

$this->registerJsFile($this->getAssetManager()->publish(Yii::getAlias('@webroot/js/rbac.js'))[1], [
    'depends' => [\frontend\assets\AppAsset::className()]
]);
$this->registerJsFile($this->getAssetManager()->publish(Yii::getAlias('@webroot/js/serializeJSON/jquery.serializejson.min.js'))[1], [
    'depends' => [\frontend\assets\AppAsset::className()]
]);
?>
<h1>Permissions</h1>
<hr/>

<?php $form = \yii\bootstrap\ActiveForm::begin(['options' => ['id' => 'hidden-form']]) ?>
<input type="hidden" name="data" value=""/>
<?php $form->end() ?>

<form id="main-form">
    <?= \yii\helpers\Html::submitInput(Yii::t('system', 'Save'), [
        'class' => 'submit btn btn-success',
    ]) ?>
    <?= \yii\helpers\Html::resetInput(Yii::t('system', 'Clear'), [
        'class' => 'clear-rules btn btn-warning',
    ]) ?>
    <?= \yii\helpers\Html::submitInput('Init user assignments', [
        'class' => 'btn btn-danger js-init-assignments-button',
    ]) ?>
    <hr/>
    <table id="rbac-structure" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>&#160;</td>
            <?php foreach ($roles as $id => $label) { ?>
                <td class="user-role">
                    <?= iconv('WINDOWS-1251', 'UTF-8', str_replace('   ', ' &#160; ', preg_replace('/(.{1})/', '$1 ', iconv('UTF-8', 'WINDOWS-1251', $label)))) ?>
                </td>
            <?php } ?>
        </tr>
        <?php foreach ($tasks as $task => $actions) { ?>
            <tr>
                <td class="task-name" data-task="<?= $task ?>"><?= $task ?></td>
                <?php foreach ($roles as $id => $label) { ?>
                    <td class='user-role' data-user-role='<?= $id ?>'>#</td>
                <?php } ?>
            </tr>
            <?php foreach ($actions as $actionName) { ?>
                <tr data-task-name="<?= $task ?>" data-operation-name="<?= $actionName ?>" class="operation-row">
                    <td class="operation-name"> - <?= $actionName ?></td>
                    <?php foreach ($roles as $id => $label) { ?>
                        <?php
                        $checked = $auth->hasChild($auth->getRole($id), $auth->getPermission($task . '/' . $actionName))
                            || $auth->hasChild($auth->getRole($id), $auth->getPermission($task));
                        ?>
                        <td data-role="<?= $id ?>" class="td-roles<?= ($checked ? ' active' : '') ?>">
                            <input type="hidden" value="<?= intval($checked) ?>"
                                   name="<?= $task ?>[<?= $actionName ?>][<?= $id ?>]"/>
                        </td>
                    <?php
                    }
                    ?>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
</form>
<style>
    #rbac-structure td {
        padding: 2px 10px;
        border: 1px solid #d8dae1;
        cursor: pointer;
    }

    #rbac-structure .user-role {
        width: 8px;
        vertical-align: bottom;
        line-height: .8em;
        padding: 10px;
    }

    #rbac-structure .active {
        background-color: #77b6e9;
    }

    #rbac-structure .td-roles:not(.active):hover,
    #rbac-structure .task-name:hover {
        background-color: #d2e4ff;
    }

    #rbac-structure .user-role {
        font-size: 12px;
        font-weight: bold;
    }
</style>