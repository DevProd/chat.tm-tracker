<?php
/**
 * @var $this yii\web\View
 * @var $project \common\models\Repository
 */

$this->title = 'Sign Up';
?>
<div class="Project-index">

    <h1>Create repository</h1>

    <hr/>
    <?php
    $form = \frontend\widgets\ActiveForm::begin();

    echo \frontend\widgets\Repository\SaveForm::fields($form, $project);
    ?>
    <hr/>
    <?php
    echo \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']);

    $form->end();
    ?>
</div>
