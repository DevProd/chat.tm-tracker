<?php
/**
 * @var $this yii\web\View
 * @var $model \common\models\Chat
 */

?>
<div class="row">
    <div class="col-lg-12">
        <div class="blocked-bg"></div>
        <?php
        $form = \frontend\widgets\ActiveForm::begin();
        echo \frontend\widgets\Chat\SaveForm::fields($form, $model);
        ?>
        <hr/>
        <?php
        echo \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']);
        $form->end();
        ?>
    </div>
</div>
