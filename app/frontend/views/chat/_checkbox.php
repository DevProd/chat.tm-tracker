<?php
/**
 * @var $this yii\web\View
 * @var $id integer
 * @var $list array
 */
?>
<?= \frontend\widgets\User\MinGridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels' => $list
    ]),
    'options' => [
        'class' => 'grid-view',
        'data-action-url' => \yii\helpers\Url::to(['/chat/grand-access', 'id' => $id]),
    ]
]) ?>
