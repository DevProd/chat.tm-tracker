<?php
use \yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $id integer|null
 */
\ext\WebSocket\Asset::register($this);

$this->title = 'Tracker chat';
?>
    <div id="chat" class="chat">
        <div class="row">
            <div class="col-xs-9">
                <div class="notice-content">
                    <div class="notice-show-more">
                        <a href="javascript://" class="btn btn-success notice-show-more-button">Show more</a>
                    </div>
                    <div class="notice-list">
                        <div class="notice-item--loader">
                            <div>
                                <div class="three-quarters-loader">Loading...</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <h4>User list</h4>

                <div class="user-content">
                    <div class="user-list">

                    </div>
                </div>
                <br/>
                <a href="/repository/hook">test</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <form id="notice-form">
                    <?= Html::label(
                        'Scroll to bottom?' . Html::checkbox('scrollToBottom', true, ['id' => 'scroll-to-bottom']),
                        'scroll-to-bottom'
                    ) ?>
                    <div class="btn-group-xs">
                        <?= Html::button('Button', ['class' => 'btn btn-success']) ?>
                        <?= Html::button('Button', ['class' => 'btn btn-success']) ?>
                        <?= Html::button('Button', ['class' => 'btn btn-success']) ?>
                        <?= Html::button('Button', ['class' => 'btn btn-success']) ?>
                        <?= Html::button('Button', ['class' => 'btn btn-success']) ?>
                        <?= Html::button('Button', ['class' => 'btn btn-success']) ?>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <?= Html::textarea('Notice[message]', '', ['class' => 'form-control']) ?>
                    <hr>
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                </form>
            </div>
            <div class="col-xs-3">
            </div>
        </div>
    </div>

    <script id="notice-item" type="text/html">
        <div class="notice-item__row">
            <div class="notice-item__id"></div>
            <div class="notice-item__type"></div>
            <div class="notice-item__owner"></div>
        </div>
        <div class="notice-item__text"></div>
        <div class="notice-item__created"></div>
    </script>

    <script id="user-item" type="text/html">
        <div class="user-item__status"><span></span></div>
        <div class="user-item__username"></div>
        <div class="user-item__role"></div>
    </script>

<?php
$this->registerJs(
    'window.app = new Application('
    . \yii\helpers\Json::encode([
        'chatId' => $id,
        'registerId' => Yii::$app->user->id,
        'sessionId' => Yii::$app->session->getId(),
        'socketOptions' => [
            'host' => 'localhost',
            'port' => 8000,
        ]
    ])
    . ');'
);