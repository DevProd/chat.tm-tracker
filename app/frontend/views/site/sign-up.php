<?php
/**
 * @var $this yii\web\View
 * @var $model \common\models\User
 */

$this->title = Yii::t('system', 'Sign Up');
?><h1><?= $this->title ?></h1><?php
$form = \frontend\widgets\ActiveForm::begin();
?>

<?= \frontend\widgets\User\SaveForm::fields($form, $model) ?>
<?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

<?php
$form->end();
