function createInput($elm) {
    if ($elm.hasClass('active')) {
        $elm.find('input').val(1);
        $elm.removeAttr('style');
    } else {
        $elm.find('input').val(0);
    }
}

$(function () {
    var $mainForm = $('#main-form'),
        color = '#d2e4ff',
        doAssign = false;

    $mainForm.submit(function () {
        var $form = $('#hidden-form');

        $form.find('input[name="data"]').val(JSON.stringify(doAssign ? {"doAssign": true} : $(this).serializeJSON()));
        $form.submit();

        return false;
    });

    /** Init users assignments */
    $('.js-init-assignments-button').click(function () {
        doAssign = true;
    });

    //--------- Выбор операции по конкретной роли
    $('.td-roles').click(function () {
        $(this).toggleClass('active', '');
        createInput($(this));
    });
    //=========

    //--------- Операция
    var $operationRow = $('.operation-name');

    $('.clear-rules').click(function () {
        if (confirm('Вы уверены что хотите очистить таблицу правил?')) {
            $('.rbac-structure').find('.active').removeClass('active').find('input').val('0');
        }
    });

    $operationRow.mouseover(function () {
        $(this).parent('.operation-row').css('background-color', color);
    });

    $operationRow.mouseleave(function () {
        $(this).parent('.operation-row').removeAttr('style');
    });

    $operationRow.click(function () {
        var $elm = $(this).parent('.operation-row').find('.td-roles');
        $elm.toggleClass('active', '');
        $elm.each(function (i, elm) {
            createInput($(elm));
        });
    });
    //=========

    //---------
    var $taskBlock = $('.task-name');

    $taskBlock.mouseover(function () {
        $('tr[data-task-name=' + $(this).data('task') + ']').css('background-color', color);
    });

    $taskBlock.mouseleave(function () {
        $('tr[data-task-name=' + $(this).data('task') + ']').removeAttr('style');
    });

    $taskBlock.click(function () {
        var $elm = $('tr[data-task-name=' + $(this).data('task') + '] td');
        $elm.not('.operation-name').toggleClass('active', '');
        $elm.each(function (i, elm) {
            createInput($(elm));
        });
    });
    //=========

    //---------
    var $roleElm = $('.user-role');

    $roleElm.mouseover(function () {
        var $elm = $(this);
        $elm.css('background-color', color);
        mouseEventForTaskByRole($elm.parent('tr'), $elm.data('user-role'), color, 'over');
    });

    $roleElm.mouseleave(function () {
        var $elm = $(this);
        $elm.removeAttr('style');
        mouseEventForTaskByRole($elm.parent('tr'), $elm.data('user-role'), color, 'out');
    });

    $roleElm.click(function () {
        if (event.ctrlKey) {
            var $elm = $('td[data-role="' + $(this).data('user-role') + '"]');
            $elm.toggleClass('active', '');
            $elm.each(function (i, elm) {
                createInput($(elm));
            });
            mouseEventForTaskByRole($(this).parent('tr'), $(this).data('user-role'), color, 'over');
        } else {
            var $td = $(this);
            checkForTask($td.parent('tr'), $td.data('user-role'), color);
        }
    });
});

function checkForTask($elm, role, color) {
    var $nextElement = $elm.next();
    if ($nextElement.hasClass('operation-row')) {
        var $td = $nextElement.find('td[data-role="' + role + '"]');
        $td.toggleClass('active', '');
        $td.filter(':not(.active)').css('background-color', color);
        $td.filter('.active').removeAttr('style');
        createInput($td);
        checkForTask($nextElement, role, color);
    }
}

function mouseEventForTaskByRole($elm, role, color, event) {
    var $nextElement = $elm.next();
    if ($nextElement.hasClass('operation-row')) {
        if (event == 'over') {
            $nextElement.find('td[data-role="' + role + '"]').not('.active').css('background-color', color);
        } else {
            $nextElement.find('td[data-role="' + role + '"]').not('.active').removeAttr('style');
        }
        mouseEventForTaskByRole($nextElement, role, color, event);
    }
}