var Events = {
    templates: {
        "notice-item": $('#notice-item').html(),
        "user-item": $('#user-item').html()
    },
    events: {
        "userInfo": function (data) {
            if (data['id'] === undefined) {
                return;
            }
            var $item = $('.user-item[data-id="' + data['id'] + '"]');

            if ($item.length) {
                if (data['online'] === true) {
                    $item.find('.user-item__status').addClass('user_online')
                } else {
                    $item.find('.user-item__status').removeClass('user_online')
                }
            }
        },
        "userList": function (data) {
            var $list = $('.user-list');

            $list.empty();

            for (var i = 0; i < data.length; i++) {
                var $tpl = $('<div/>', {
                        'class': 'user-item',
                        'data-id': data[i]['id'],
                        'data-role': data[i]['role'],
                        'data-online': data[i]['online']
                    }).html(Events.templates['user-item']),
                    online = data[i]['online'];

                if (online) {
                    $tpl.find('.user-item__status').addClass('user_online');
                }

                $tpl.find('.user-item__username').html(data[i]['username']);
                $tpl.find('.user-item__role').html(data[i]['roleText']);

                $list.append($tpl);
            }
        },
        "noticeList": function (data, isNew, prepend) {
            var l = data.length,
                $list = $('.notice-list');

            isNew = isNew !== undefined && isNew === true;
            prepend = prepend !== undefined && prepend === true;

            $('.notice-item--loader').fadeOut(200, function() {
                $(this).remove()
            });

            var timeoutId = setTimeout(function() {
                for (var i = 0; i < data.length; i++) {
                    var id = data[i]['id'],
                        owner_id = data[i]['owner_id'],
                        $tpl = $('<div/>', {
                            'class': 'notice-item',
                            'data-id': id,
                            'data-owner-id': Number(owner_id) === +owner_id && +owner_id % 1 === 0 ? owner_id : null,
                            'data-type': data[i]['type']
                        }).html(Events.templates['notice-item']);

                    $tpl.find('.notice-item__id').html("<span class='badge'>#" + id + "</span>");
                    $tpl.find('.notice-item__type').html(data[i]['typeText']);
                    $tpl.find('.notice-item__owner').html(
                        "<a class='badge btn-success' href='#'>" + data[i]['ownerText'] + "</a>"
                    );
                    $tpl.find('.notice-item__text').html(data[i]['message']);
                    $tpl.find('.notice-item__created').text(data[i]['created_at']);

                    if (isNew) {
                        localStorage.setItem('new-' + id, id);
                    }

                    if (localStorage.getItem('new-' + id)) {
                        $tpl.addClass('notice-item--new').on('mouseover', function () {
                            localStorage.removeItem(
                                'new-' + $(this).removeClass('notice-item--new').off('mouseover').data('id')
                            );
                        });
                    }

                    if(prepend) {
                        $list.prepend($tpl.fadeIn(100));
                    } else {
                        $list.append($tpl.fadeIn(100));
                    }
                    l--;
                }

                var timer = setInterval(function () {
                    if (l <= 0) {
                        if(prepend) {
                            $('.notice-content').scrollTo(0);
                        } else {
                            $('.notice-content').scrollTo($list.height());
                        }
                        clearInterval(timer);
                    }
                }, 100);

                clearTimeout(timeoutId);
            }, 200);
        },
        "newNotices": function (data) {
            if (!data) {
                return;
            }

            var audio = new Audio('../../sounds/sound_2.mp3');
            audio.play();

            this["noticeList"](data, true);
        },
        "prevNoticeList": function (data) {
            if (!data) {
                return;
            }

            this["noticeList"](data, false, true);
        },
        "Exception": function (error) {
            console.log(error);
        }
    },
    on: function (event, data) {
        this.events[event](data);
    }
};