function Application(opts) {
    var self = this;

    self = $.extend({
        chatId: null,
        sessionId: null,
        registerId: null,
        socketOptions: {
            host: null,
            port: null
        }
    }, opts);

    self.queue = [];
    self.__renderRunned = false;

    self.connect = new WebSocketClient($.extend(
        {
            host: self.socketOptions.host,
            port: self.socketOptions.port,
            onOpen: function () {
                self.send({
                    register: {
                        registerId: self.registerId,
                        chatId: self.chatId
                    }
                }, function () {
                    self.send({
                        method: "chatInit",
                        params: {
                            chatId: self.chatId,
                            public: false
                        }
                    })
                });
            },
            onMessage: function (message) {
                console.log(message);

                $.each(message, function (event, data) {
                    if(event == 'callback') {
                        if (self.queue[data]) {
                            self.queue[data]();
                            delete self.queue[data];
                        }
                    } else {
                        Events.on(event, data);
                    }
                });
            }
        },
        self.connect
    ));

    self.send = function (data, callback) {
        if (callback instanceof Function) {
            self.queue.push(callback);
            data['callback'] = self.queue.length - 1;
        }

        self.connect.send(JSON.stringify(data));
    };

    self.windowSizing = function () {
        $('.notice-content').height(+$(window).height() - +$('#chat').find('> div:last-child').height());
    };

    $(window).on('resize', self.windowSizing);

    $('#notice-form').on('submit', function() {
        var $textarea = $(this).find('textarea');

        if($textarea.val() != '') {
            self.send({
                method: "sendMessage",
                params: {
                    text: $textarea.val(),
                    chatId: self.chatId
                }
            });
            $textarea.val('');
        }


        return false;
    });

    $('.notice-show-more-button').on('click', function() {
        var firstId = $('.notice-item:first-child').data('id');

        console.log(firstId);
        self.send({
            method: "getPreviousMessage",
            params: {
                firstId: firstId
            }
        });
    });

    self.windowSizing();

    return self;
}