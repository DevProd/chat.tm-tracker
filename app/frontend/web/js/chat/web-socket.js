/**
 * Created by User on 04.12.2014.
 */
function WebSocketClient(opts) {
	var o = $.extend(
		{
			protocol: 'ws',
			host: 'localhost',
			port: 8000,
			registerId: null,
			sessionId: null,
			onOpen: function (data) {},
			onMessage: function (data) {}
		}, opts
	);
	var socket = new WebSocket(o.protocol + '://' + o.host + ':' + o.port + '/');
	socket.onopen = function (msg) {
		o.onOpen(msg);
	};
	socket.onmessage = function (msg) {
		if (msg.data) {
			var data = $.parseJSON(msg.data);
			o.onMessage(data);
		}
	};
	socket.onclose = function (msg) {
		console.log('Reconnect...');

		setTimeout(function () {
			//location.reload();
		}, 1000);
	};
	return socket;
}
