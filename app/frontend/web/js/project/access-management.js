$(function() {
    var accessManagement = {
        run: function () {
            var timeoutTimer;

            $('#user-management-form').find('input[type="text"]').on('keyup', function () {
                var $input = $(this),
                    $form = $input.closest('form');

                if (timeoutTimer) {
                    clearTimeout(timeoutTimer);
                    timeoutTimer = null;
                }

                timeoutTimer = setInterval(function () {
                    $.pjax.reload({
                        container: '#user-management-list',
                        url: $form.prop('action') + '?' + $form.serialize(),
                        replace: true
                    });

                    $('#user-management-list').on('pjax:end', function () {
                        clearTimeout(timeoutTimer);
                        timeoutTimer = null;
                        $input.focus();
                    });
                }, 1000);
            });
        }
    };

    accessManagement.run();
});
