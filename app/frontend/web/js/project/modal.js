$(function () {

    $('#create-chat-modal').on('shown.bs.modal', function () {
        var $modal = $(this);

        $.get($('[data-target="#create-chat-modal"]').data('action-url'), function (response) {
            var $body = $modal.find('.modal-body');

            $body.html(response);
            $body.find('form').on('submit', function () {
                var $form = $(this);

                $.get($form.prop('action'), $form.serialize(), function (data) {
                    var hasErrors = false;

                    $form.find('.help-block').empty();

                    for (var prop in data) {
                        if (data.hasOwnProperty(prop)) {
                            var $block = $form.find('.field-' + prop + ' .help-block');

                            if ($block.length && data[prop] instanceof Array) {
                                hasErrors = true;
                                for (var i = 0; i < data[prop].length; i++) {
                                    $block.append(data[prop][i]);
                                }
                            }
                        }
                    }

                    if (!hasErrors) {
                        $modal
                            .find('[data-dismiss="modal"]')
                            .click()
                            .closest('.modal-content')
                            .find('.modal-body')
                            .empty()
                        ;
                        $.pjax.reload({container: "#chat-update"});
                        $("#chat-update").on("pjax:end", function () {
                            $(this).find('a.chat-manage-access-js').off('click').on('click', function (e) {
                                e.preventDefault();
                                ManageAccessModal($(this));
                            });
                        });
                    }
                }, "json");

                return false;
            });
        });
    });

    $('a.chat-manage-access-js').on('click', function () {
        ManageAccessModal($(this));
    });

});

function ManageAccessModal($elm) {
    var $modal = $('#chat-manage-access');

    $modal.find('.modal-header h3').text($elm.data('header-text'));
    $.get($elm.data('action-url'), function (html) {
        var $body = $modal.find('.modal-body');

        $body.html(html);

        $body.find('input[type="checkbox"][name="grand-access-all"]').on('change', function () {

            $.get($body.find('.grid-view').data('action-url'), {
                toAll: this.name == 'grand-access-all',
                userId: $(this).data('user-id')
            }, function (data) {

            }, "json");

        });

        $body.find('input[type="checkbox"][name="grand-access-one"]').on('change', function () {
            console.log(this.name);
            console.log($(this).data('user-id'));

            $.get($body.find('.grid-view').data('action-url'), {
                toAll: this.name == 'grand-access-one',
                userId: $(this).data('user-id')
            }, function (data) {

            }, "json");

        });

    });
}