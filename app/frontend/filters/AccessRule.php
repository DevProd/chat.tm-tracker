<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.10.2014
 * Time: 18:46
 */

namespace frontend\filters;

use Yii;
use yii\base\Action;
use yii\web\Controller;
use yii\web\Request;
use yii\web\User;

class AccessRule extends \yii\filters\AccessRule
{
	public $allow = true;

	/**
	 * Checks whether the Web user is allowed to perform the specified action.
	 * @param Action $action the action to be performed
	 * @param User $user the user object
	 * @param Request $request
	 * @return boolean|null true if the user is allowed, false if the user is denied, null if the rule does not apply to the user
	 */
	public function allows($action, $user, $request)
	{
		if ($this->matchAction($action)
			&& $this->matchRole($user)
			&& $this->matchRoleForAction($user, $action)
			&& $this->matchIP($request->getUserIP())
			&& $this->matchVerb($request->getMethod())
			&& $this->matchController($action->controller)
			&& $this->matchCustom($action)
		) {
			return $this->allow ? true : false;
		} else {
			return null;
		}
	}

	/**
	 * @param User $user the user object
	 * @param Action $action the action to be performed
	 * @return boolean whether the rule applies to the role
	 */
	protected function matchRoleForAction($user, $action)
	{
		return $user->can($action->getUniqueId());
	}

} 