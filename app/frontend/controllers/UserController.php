<?php
namespace frontend\controllers;

use frontend\filters\AccessRule;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * Class User
 */
class UserController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'class' => AccessRule::className(),
                    ],
                ],
            ],
        ];
	}

	/**
	 * @return string
	 */
	public function actionIndex()
	{
		return $this->render('index');
	}
}