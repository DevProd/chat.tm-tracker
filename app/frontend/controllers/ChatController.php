<?php
namespace frontend\controllers;

use common\models\Chat;
use common\models\ChatHasUser;
use common\models\Project;
use common\models\User;
use frontend\filters\AccessRule;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class ChatController
 */
class ChatController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'class' => AccessRule::className(),
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @return string enable_repository_events
     */
    public function actionIndex($id)
    {
        $this->layout = 'chat.php';

        return $this->render('index', ['id' => $id]);
    }

    /**
     * @param $id
     * @param $toAll
     * @param $userId
     * @return string
     */
    public function actionGrandAccess($id, $toAll, $userId = null)
    {
        VarDumper::dump(Yii::$app->user->id);

        return '';
    }

    /**
     * @param $id
     * @return string
     * @throws BadRequestHttpException
     */
    public function actionManageAccess($id)
    {
        if (!Yii::$app->getRequest()->isAjax) {
            throw new BadRequestHttpException("Not found!", 404);
        }

        /** @var Chat $chat */
        if (!($chat = Chat::findOne($id))) {
            throw new BadRequestHttpException("Not found!", 404);
        }

        $old = $chat->getUserByAccess()->asArray()->all();
        $oldIds = array_values(ArrayHelper::map($old, 'id', 'id'));
        $new = User::find()
            ->where(['not in', 'id', $oldIds])
            ->andWhere(['status' => User::STATUS_ACTIVE])
            ->asArray()->all();

        $list = ArrayHelper::merge($new, $old);

        foreach ($list as $k => $val) {
            $list[$k]['checked'] = in_array($val['id'], $oldIds);
        }

        usort($list, function ($a, $b) {
            return (int)$a['id'] > (int)$b['id'];
        });

        return $this->renderPartial('_checkbox', [
            'id' => $chat->id,
            'list' => $list
        ]);
    }

}