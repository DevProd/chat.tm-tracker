<?php
namespace frontend\controllers;

use common\models\Project;
use common\models\User;
use Yii;
use common\forms\Login;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => false,
                        //'allow' => true,
                        //'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if(!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return string
     */
    public function actionSignUp()
    {
        $model = new User(['scenario' => 'sign-up']);

        if ($model->load(Yii::$app->request->post(), 'User')) {
            $authManager = Yii::$app->getAuthManager();
            $transaction = $model->getDb()->beginTransaction();
            $password = $model->password_hash;
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
            $model->status = User::STATUS_ACTIVE;
            $model->role = $model->role ?: User::ROLE_DEVELOPER;

            if ($model->save()) {
                $transaction->commit();
                $login = new Login();
                $login->username = $model->username;
                $login->password = $password;
                $login->rememberMe = true;

                $authManager->assign($authManager->getRole($model->role), $model->id);

                if($login->login()) {
                    return $this->redirect(Url::to('/'));
                } else {
                    return $this->redirect(Url::to('/site/login'));
                }
            } else {
                $transaction->rollBack();
                $model->password_hash = $password;
            }
        }

        return $this->render('sign-up', ['model' => $model]);
    }


}
