<?php
namespace frontend\controllers;

use common\models\Repository;
use ext\Repository\Helper;
use ext\WebSocket\Client;
use frontend\filters\AccessRule;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * Class RepositoryController
 */
class RepositoryController extends Controller
{
    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'class' => AccessRule::className(),
//                    ],
//                ],
//            ],
//        ];
//    }

    /**
     * @param integer
     * @return string
     */
    public function actionCreate($pid)
    {
        $project = new Repository();

        if (is_numeric($pid) && $project->load(Yii::$app->request->post())) {
            $project->status = Repository::STATUS_ACTIVE;
            $project->owner_id = Yii::$app->getUser()->getId();
            $project->project_id = $pid;

            if ($project->save()) {
                return $this->redirect(Url::to(['/project/view', 'id' => $pid]));
            }
        }

        return $this->render('edit', ['project' => $project]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionHook()
    {
        switch(Yii::$app->getRequest()->getUserAgent()) {
            case 'Bitbucket-Webhooks/2.0':
                $type = Repository::REPOSITORY_BITBUCKET;
                break;
            case 'GitHub-Hookshot/cd33156':
                $type = Repository::REPOSITORY_GITHUB;
                break;
            default:
                throw new ForbiddenHttpException('Вам не разрешено производить данное действие');
        }

        $json = Yii::$app->getRequest()->getRawBody();

        //file_put_contents(__DIR__ . '/__get_github.json', $json);
        //$json = file_get_contents(__DIR__ . '/__get_github.json');

        if (!empty($json)) {
            $host = Yii::$app->params['WebSocketServer-host'] ?: 'localhost';
            $port = Yii::$app->params['WebSocketServer-port'] ?: 8000;

            if($type == Repository::REPOSITORY_BITBUCKET) {
                $info = Helper::trimByKey(json_decode($json, true), 'uuid', '{}');
            } else {
                $info = json_decode($json, true);
            }

            $info['type'] = $type;

            $client = new Client("ws://" . $host . ":" . $port . "/");
            $client->send(json_encode([
                'hookFileCache' => [
                    'alias' => $info['repository']['full_name'],
                    'fileName' => Helper::cacheData(json_encode($info)),
                ],
            ]));
            echo $client->receive();
        }
    }

}