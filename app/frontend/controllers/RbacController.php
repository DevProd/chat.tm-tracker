<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.10.2014
 * Time: 9:13
 */

namespace frontend\controllers;


use common\components\RbacHelper;
use common\models\User;
use Yii;
use yii\base\Exception;
use yii\helpers\VarDumper;
use yii\rbac\Item;
use yii\rbac\Rule;
use yii\web\Controller;

class RbacController extends Controller
{
    public $itemRuleNamespace = 'common\\config\\rbac\\rules\\';

    public function actionIndex()
    {
        $auth = Yii::$app->getAuthManager();
        $data = json_decode(Yii::$app->request->post('data'), true);

        /** Update .../frontend/rbac/rules.php */
        foreach(glob(Yii::getAlias('@' . str_replace('\\', '/', $this->itemRuleNamespace)) . '*ItemRule.php') as $fileName) {
            /** @var Rule $rule */
            $ruleClass = $this->itemRuleNamespace . preg_replace('/.+[\/](\w+)\.php$/i', '$1', $fileName);
            $rule = new $ruleClass();

            if(null === $auth->getRule($rule->name)) {
                $auth->add($rule);
            } else {
                unset($rule);
            }
        }

        if ($data) {
            $roleObjects = $auth->getRoles();

            foreach ($data as $controllerName => $actions) {
                $thisRoles = [];

                foreach ($actions as $actionName => $roles) {
                    foreach ($roles as $roleId => $checked) {
                        $thisRoles[$roleId][$actionName] = (bool)$checked;
                    }
                }

                foreach ($thisRoles as $thisRoleId => $thisRoleActions) {
                    $controllerRole = $auth->getPermission($controllerName);

                    if (sizeof(array_filter($thisRoleActions, function ($val) {
                            return !$val;
                        })) == 0
                    ) {
                        if (!$auth->hasChild($roleObjects[$thisRoleId], $controllerRole)) {
                            $auth->addChild($roleObjects[$thisRoleId], $controllerRole);
                        } else {
                            /** @var Item[] $children */
                            $children = $auth->getChildren($controllerName);
                            foreach($children as $permission) {
                                $auth->removeChild($roleObjects[$thisRoleId], $permission);
                            }
                        }
                    } else {
                        if ($auth->hasChild($roleObjects[$thisRoleId], $controllerRole)) {
                            $auth->removeChild($roleObjects[$thisRoleId], $controllerRole);
                        }
                        foreach ($thisRoleActions as $thisActionName => $checked) {
                            $childPermissionName = $controllerName . '/' . $thisActionName;
                            $thisPermission = $auth->getPermission($childPermissionName);

                            if ($thisPermission) {
                                if ($auth->hasChild($roleObjects[$thisRoleId], $thisPermission)) {
                                    if (!$checked) {
                                        $auth->removeChild($roleObjects[$thisRoleId], $thisPermission);
                                    }
                                } else {
                                    if ($checked) {
                                        $auth->addChild($roleObjects[$thisRoleId], $thisPermission);
                                    }
                                }
                            } else {
                                throw new Exception('Not found permission "' . $childPermissionName . '"');
                            }
                        }

                    }
                }
            }
        }
        return $this->render('permissions', [
            'roles' => User::getRoles(),
            'tasks' => RbacHelper::getTasks()
        ]);
    }
}