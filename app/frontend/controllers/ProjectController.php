<?php
namespace frontend\controllers;

use common\models\Chat;
use common\models\ChatHasUser;
use common\models\Project;
use common\models\Repository;
use common\models\User;
use frontend\filters\AccessRule;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class ProjectController
 */
class ProjectController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'class' => AccessRule::className(),
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
	}

	/**
	 * @return string
	 */
	public function actionIndex()
	{
        return $this->render('index');
	}

    /**
     * @param null $id
     * @return string|Response
     */
	public function actionCreate($id = null)
	{
        if(!$id || null === ($project = Project::findOne($id))) {
            $project = new Project();
        }

        if($project->load(Yii::$app->request->post())) {
            $project->status = Project::STATUS_ACTIVE;
            $project->owner_id = Yii::$app->getUser()->getId();

            if($project->save()) {
                return $this->redirect(Url::to('index'));
            }
        }

		return $this->render('edit', ['project' => $project]);
	}

    /**
     * @param $id
     * @return string|Response
     */
	public function actionUpdate($id)
	{
		return $this->actionCreate($id);
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function actionView($id)
	{
        $project = Project::findById($id);

        return $this->render('view', ['model' => $project]);
	}

    /**
     * @param $id
     * @return string
     * @throws BadRequestHttpException
     */
    public function actionCreateChat($id)
    {
        if(!Yii::$app->getRequest()->isAjax) {
            throw new BadRequestHttpException("Not found!", 404);
        }

        $project = Project::findById($id);

        $chat = new Chat([
            'project_id' => $project->id,
            'owner_id' => Yii::$app->user->getId(),
            'name' => $project->name . ' - Chat',
        ]);

        $chat->setScenario('create-chat');

        if($chat->load(Yii::$app->request->get())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $chat->status = User::STATUS_ACTIVE;
            $errors = ActiveForm::validate($chat, $chat->attributes());

            if(empty($errors) && $chat->save()) {
                $chatUser = new ChatHasUser();
                $chatUser->chat_id = $chat->id;
                $chatUser->user_id = $chat->owner_id;
                $chatUser->save();
            }

            return $errors;
        }

        return $this->renderPartial('/chat/edit', ['model' => $chat]);
    }


}